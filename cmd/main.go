package main

import (
	"bitbucket.org/merqueo/localpipeline/internal/di"
)

func main() {
	e, err := di.Executor()
	if err != nil {
		panic(err)
	}
	err = e.Execute()
	if err != nil {
		panic(err)
	}
}
