# Changelog

## 0.6.2

* Update ssh key to connection with bitbucket

## 0.6.1

* Update to golang 1.19

## 0.5.0

*   Refactor DI for planner
*   Remove deprecate code
*   Test coverage 70%
*   Default memory per service
*   Max memory per service
*   Min memory per service

--

## 0.4.0

*   Compile using GOAMD64=v2
*   Support ssh host fingerprint
*   Add by default bitbucket.org ssh host fingerprint
*   Support global ssh private key
*   Fix after-script
*   Dependency Injection
*   Refactor in planner function

--

## 0.3.0

*   Dependency Injection
*   Services now share loopback with the step container
*   Refactor of main.go
*   Refactor of flags
*   Cache of recent docker images pulled
*   Coverage 55%

--

## 0.2.5

*   Add unit testing
*   Publish release in slack

--

## 0.2.4

*   Add unit testing
