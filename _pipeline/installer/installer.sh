#!/bin/bash
#
# Downloads a precompiled copy of Please from our s3 bucket and installs it.
#
# curl -fsSL https://bitbucket.org/merqueo/localpipeline/raw/master/_pipeline/installer/installer.sh | VERSION=v0.0.0 bash
#

set -e

echo -e "\n"

if [ -z $VERSION ]; then
    echo "VERSION is undefined"
    exit 1
fi

# Find the os / arch to download. You can do this quite nicely with go env
# but we use this script on machines that don't necessarily have Go itself.
OS=`uname`
if [ "$OS" = "Linux" ]; then
    GOOS="linux"
elif [ "$OS" = "Darwin" ]; then
    GOOS="darwin"
else
    echo "Unknown operating system $OS"
    exit 1
fi

ARCH=`uname -m`
if [ "$ARCH" = "x86_64" ]; then 
    ARCH="amd64"
elif [ "$ARCH" = "arm64" ]; then
    :
else
    echo "Unsupported cpu arch $ARCH"
    exit 1
fi

BIN_URL="https://static.merqueo.com/binaries/localpipeline/localpipeline-${GOOS}-${ARCH}-${VERSION}"

echo -e "Downloading $BIN_URL \n"

LOCATION=${HOME}/.local/bin
mkdir -p $LOCATION
LOCATION=$LOCATION/localpipeline
rm -rf $LOCATION

curl -fsSL $BIN_URL -o $LOCATION
chmod +x $LOCATION

echo "Localpipeline installed in '$LOCATION'"
echo ""
echo "run 'localpipeline -version' to validate command availability"
echo ""
echo ""
echo "SI NO funciona, por favor reiniciar el pc"
