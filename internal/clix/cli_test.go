package clix

import (
	"fmt"
	"reflect"
	"testing"
)

func TestNewCli(t *testing.T) {
	type args struct {
		args OsArgs
	}
	tests := []struct {
		name    string
		args    args
		want    Cli
		wantErr bool
	}{
		{
			name: "ok version",
			args: args{
				args: OsArgs{
					"-version",
				},
			},
			want: Cli{
				Stop:   true,
				Runner: "testing",
			},
			wantErr: false,
		},
		{
			name: "ok empty",
			args: args{
				args: OsArgs{},
			},
			want: Cli{
				Stop:   false,
				Runner: "testing",
			},
			wantErr: false,
		},
		{
			name: "ok runner",
			args: args{
				args: OsArgs{
					"-runner",
					"xxxx",
				},
			},
			want: Cli{
				Stop:   false,
				Runner: "xxxx",
			},
			wantErr: false,
		},
		{
			name: "fail",
			args: args{
				args: OsArgs{
					"-version",
					"-runner",
				},
			},
			want:    Cli{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewCli(tt.args.args)
			fmt.Printf("%#v\n", err)
			fmt.Printf("%#v\n", got)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewCli() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCli() = %v, want %v", got, tt.want)
			}
		})
	}
}
