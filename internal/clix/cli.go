package clix

import (
	"flag"
	"fmt"
	"runtime"
)

// Version value passed at compile time
var Version string

// OsArgs parameters from cli without the command
type OsArgs []string

// Cli config load from cli parameters
type Cli struct {
	Stop   bool
	Runner string
}

// NewCli process args and return the config
func NewCli(args OsArgs) (Cli, error) {

	fs := flag.FlagSet{}
	runner := fs.String("runner", "testing", "runner name")
	version := fs.Bool("version", false, "show localpipeline version")
	err := fs.Parse(args)
	if err != nil {
		return Cli{}, err
	}

	c := Cli{
		Runner: *runner,
	}

	if *version {
		fmt.Println("localpipeline version:", Version, "os:", runtime.GOOS+"/"+runtime.GOARCH, "go:", runtime.Version())
		c.Stop = true
	}

	return c, nil
}
