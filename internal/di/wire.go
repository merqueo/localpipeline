//go:build wireinject
// +build wireinject

package di

import (
	"bitbucket.org/merqueo/localpipeline/internal/executor"
	"github.com/google/wire"
)

func Executor() (*executor.Executor, error) {
	wire.Build(set)
	return &executor.Executor{}, nil
}
