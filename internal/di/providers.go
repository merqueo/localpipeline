package di

import (
	"os"
	"os/user"

	"bitbucket.org/merqueo/localpipeline/internal/clix"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"github.com/sirupsen/logrus"
)

func loggerProvider() *logrus.Logger {
	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)
	logger.SetFormatter(&logrus.TextFormatter{
		DisableTimestamp: true,
	})
	return logger
}

func configProvider(w config.WorkDir, cli clix.Cli, user *user.User) (*config.Config, error) {
	if cli.Stop {
		os.Exit(0) // TODO esto es horrible, pensar en algo
	}

	return config.Load(cli.Runner, w, user)
}

func osArgsProvider() clix.OsArgs {
	return os.Args[1:]
}

func workdirProvider() (config.WorkDir, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return config.WorkDir(pwd), nil
}
