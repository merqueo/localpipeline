package di

import (
	"os/user"

	"bitbucket.org/merqueo/localpipeline/internal/clix"
	"bitbucket.org/merqueo/localpipeline/internal/executor"
	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"bitbucket.org/merqueo/localpipeline/internal/runner"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/remote"
	"github.com/google/wire"
)

var set = wire.NewSet(
	clix.NewCli,
	configProvider,
	workdirProvider,
	loggerProvider,
	osArgsProvider,
	user.Current,

	executor.NewExecutor,
	wire.Bind(new(executor.Docker), new(*runner.Docker)),

	load.Load,

	planner.LoadEnvsVars,
	planner.NewPlanner,
	wire.Bind(new(planner.StepBuilderInterface), new(*planner.StepBuilder)),
	wire.Bind(new(planner.TemplaterInterface), new(*planner.Templater)),
	wire.Bind(new(planner.FilerInterface), new(*planner.Filer)),
	wire.Value(&planner.Templater{}),
	wire.Value(&planner.Filer{}),
	wire.Struct(new(planner.StepBuilder), "*"),

	remote.Load,

	runner.NewDocker,
)
