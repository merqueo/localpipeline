package planner

import (
	"errors"
	"fmt"
	"io/fs"
	"testing"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/remote"
	"github.com/d4l3k/messagediff"
	"github.com/stretchr/testify/mock"
)

type StepBuilderMock struct {
	mock.Mock
}

func (m *StepBuilderMock) New(bs *remote.Step) (*Step, error) {
	args := m.Called(bs)
	return args.Get(0).(*Step), args.Error(1)
}

func TestNewPlanner(t *testing.T) {

	c := &config.Config{
		RunnerName:   "runner",
		RunnerBranch: "master",
		RunnerSteps:  []string{"r1"},
	}

	r := load.Runners{
		load.Runner{
			Name:   "runner",
			Branch: "master",
			Steps: load.StepList{
				load.Step{
					Name: "r1",
				},
			},
		},
	}

	type args struct {
		c          *config.Config
		remoteYaml remote.Config
		localYaml  load.Config
		builder    *StepBuilderMock
		filer      *FilerInterfaceMock
	}
	tests := []struct {
		name    string
		args    args
		mocker  func(args)
		want    []*Step
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				c: c,
				localYaml: load.Config{
					Repository: load.Envs{
						Vars: load.EnvMap{
							"REPO": "1",
						},
					},
					Deployments: load.Deployments{
						"deployment-1": load.Envs{
							Vars: load.EnvMap{
								"DEPLO": "1",
							},
						},
					},
					SSH: load.SSH{
						Hosts: []string{
							"xxxxx",
						},
						Key: "yyyyy",
					},
					Runners: load.Runners{
						load.Runner{
							Name:   "runner",
							Branch: "master",
							Steps: load.StepList{
								load.Step{
									Name: "r1",
									Mounts: []string{
										"vendor",
									},
								},
							},
						},
					},
				},
				remoteYaml: remote.Config{
					Definitions: remote.Definitions{
						Services: remote.Services{
							"mysql": remote.Service{
								Image: "mysql:5.7",
							},
						},
						Caches: map[string]string{
							"x-composer": "~/.composer",
						},
					},
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name:       "r1",
										Deployment: "deployment-1",
										Services: []string{
											"mysql",
										},
										Caches: []string{
											"x-composer",
										},
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {

				a.filer.On("MkdirAll", "/work/vendor", fs.FileMode(0755)).Return(nil).Once()
				a.filer.On("MkdirAll", "/st/mounts/vendor", fs.FileMode(0755)).Return(nil).Once()
				a.filer.On("MkdirAll", "/st/cache/x-composer", fs.FileMode(0755)).Return(nil).Once()

				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{
						HostConfig: HostConfig{
							WorkDir:       "/work",
							StoreMountDir: "/st/mounts",
							StoreCacheDir: "/st/cache",
							StoreEntryDir: "/st/entry",
						},
						Env: load.EnvMap{
							BitbucketCloneDir: "/clone",
						},
						MountsCommon: []string{},
						MountsMain:   []string{},
						MountsPipe:   []string{},
						filer:        a.filer,
					}, nil).
					Once()
			},
			want: []*Step{
				{
					Env: load.EnvMap{
						BitbucketCloneDir: "/clone",
						"CI":              "true",
						"LOCAL_CI":        "true",
						"REPO":            "1",
						"DEPLO":           "1",
					},
					HostConfig: HostConfig{
						WorkDir:       "/work",
						StoreMountDir: "/st/mounts",
						StoreCacheDir: "/st/cache",
						StoreEntryDir: "/st/entry",
					},
					ImageTag: "latest",
					MountsCommon: []string{
						"/st/cache/x-composer:~/.composer",
						"/st/mounts/vendor:/clone/vendor",
					},
					MountsMain: []string{
						"yyyyy:$HOME/.ssh/id_rsa:ro",
					},
					MountsPipe: []string{
						"yyyyy:/opt/atlassian/pipelines/agent/ssh/id_rsa_tmp:ro",
					},
					RAM: 3032481792,
					Services: []Service{
						{
							ContainerName:   "-service-mysql",
							ImageRepository: "mysql",
							ImageTag:        "5.7",
							Env:             []string{},
							RAM:             1262485504,
						},
					},
					SSHHosts: []string{
						"xxxxx",
						bitbucketFingerPrint,
					},
				},
			},
			wantErr: false,
		},
		{
			name: "ok - ssh host bitbucket only",
			args: args{
				c: c,
				localYaml: load.Config{
					SSH: load.SSH{
						Hosts: []string{
							"bitbucket.org yyyyy",
						},
					},
					Runners: r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name: "r1",
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{
						Env:          load.EnvMap{},
						MountsCommon: []string{},
						MountsMain:   []string{},
						MountsPipe:   []string{},
						filer:        a.filer,
					}, nil).
					Once()
			},
			want: []*Step{
				{
					Env: load.EnvMap{
						"CI":       "true",
						"LOCAL_CI": "true",
					},
					ImageTag:     "latest",
					MountsCommon: []string{},
					MountsMain:   []string{},
					MountsPipe:   []string{},
					RAM:          4294967296,
					SSHHosts: []string{
						"bitbucket.org yyyyy",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "fail process repository vars",
			args: args{
				localYaml: load.Config{
					Repository: load.Envs{
						Files: []string{
							"/xxxxxxxxxxx",
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker:  func(a args) {},
			wantErr: true,
		},
		{
			name: "fail creating step",
			args: args{
				c: c,
				localYaml: load.Config{

					Runners: r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name: "r1",
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return((*Step)(nil), errors.New("ups")).
					Once()
			},
			wantErr: true,
		},
		{
			name: "fail registering cache",
			args: args{
				c: c,
				localYaml: load.Config{

					Runners: r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name: "r1",
										Caches: []string{
											"xxxxx",
										},
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{}, nil).
					Once()
			},
			wantErr: true,
		},
		{
			name: "fail registering services",
			args: args{
				c: c,
				localYaml: load.Config{
					Runners: r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name: "r1",
										Services: []string{
											"xxxxx",
										},
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{}, nil).
					Once()
			},
			wantErr: true,
		},
		{
			name: "fail deployment does not exists",
			args: args{
				c: c,
				localYaml: load.Config{
					Deployments: load.Deployments{},
					Runners:     r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name:       "r1",
										Deployment: "xxxxx",
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{}, nil).
					Once()
			},
			wantErr: true,
		},
		{
			name: "fail deployment invalid file",
			args: args{
				c: c,
				localYaml: load.Config{
					Deployments: load.Deployments{
						"xxxxx": load.Envs{
							Files: []string{
								"/xxxxxxxxxxxx",
							},
						},
					},
					Runners: r,
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name:       "r1",
										Deployment: "xxxxx",
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{}, nil).
					Once()
			},
			wantErr: true,
		},
		{
			name: "fail runner mount",
			args: args{
				c: c,
				localYaml: load.Config{
					Runners: load.Runners{
						load.Runner{
							Name:   "runner",
							Branch: "master",
							Steps: load.StepList{
								load.Step{
									Name: "r1",
									Mounts: []string{
										"/xxxxxxx",
									},
								},
							},
						},
					},
				},
				remoteYaml: remote.Config{
					Pipelines: remote.Pipelines{
						Branches: map[string]remote.Branch{
							"master": {
								remote.BranchStep{
									Step: &remote.Step{
										Name: "r1",
									},
								},
							},
						},
					},
				},
				builder: &StepBuilderMock{},
				filer:   &FilerInterfaceMock{},
			},
			mocker: func(a args) {
				a.builder.On("New", a.remoteYaml.Pipelines.Branches["master"][0].Step).
					Return(&Step{}, nil).
					Once()
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.want != nil {
				tt.want[0].filer = tt.args.filer
			}
			tt.mocker(tt.args)
			got, err := NewPlanner(tt.args.c, tt.args.remoteYaml, tt.args.localYaml, tt.args.builder)
			fmt.Println(err)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewPlanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff, equal := messagediff.PrettyDiff(tt.want, got); !equal {
				t.Errorf("Something() = %#v\n%s", got, diff)
			}

			tt.args.builder.AssertExpectations(t)
			tt.args.filer.AssertExpectations(t)
		})
	}
}
