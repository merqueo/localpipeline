package planner

import (
	"fmt"
	"strings"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/remote"
)

type StepBuilderInterface interface {
	New(bs *remote.Step) (*Step, error)
}

// NewPlanner generate the plan to execute
func NewPlanner(c *config.Config, remoteYaml remote.Config, localYaml load.Config, builder StepBuilderInterface) ([]*Step, error) {

	// phase 1, load repository vars
	repositoryVars, err := localYaml.Repository.Process()
	if err != nil {
		return nil, err
	}

	// phase 2, filter steps from bitbucket-pipeline.yml
	remoteSteps := remoteYaml.Pipelines.Steps(c.RunnerBranch, c.RunnerTag, c.RunnerSteps)
	steps := make([]*Step, len(remoteSteps))
	for i, bs := range remoteSteps {
		// create new planner step
		s, err := builder.New(bs)
		if err != nil {
			return nil, err
		}
		steps[i] = s

		// if step not defined image then use main image
		if s.ImageRepository == "" {
			s.ImageRepository = remoteYaml.Image.Name()
			s.ImageTag = remoteYaml.Image.Tag()
		}

		// add caches to the step
		err = s.addCaches(bs.Caches, remoteYaml.Definitions.Caches)
		if err != nil {
			return nil, err
		}

		// add services to the step
		err = s.addServices(bs.Services, remoteYaml.Definitions.Services, bs.Size)
		if err != nil {
			return nil, err
		}

		// add repository vars to the step
		s.Env.Add(repositoryVars)

		// add deployment vars to the step
		if bs.Deployment != "" {
			if deploymentVars, ok := localYaml.Deployments[bs.Deployment]; ok {
				vars, err := deploymentVars.Process()
				if err != nil {
					return nil, err
				}
				s.Env.Add(vars)
			} else {
				return nil, fmt.Errorf("deployment %s undefined", bs.Deployment)
			}
		}

		// add ssh hosts fingerprints to the step
		addBitbucketFingerPrint := true
		for _, h := range localYaml.SSH.Hosts {
			if strings.HasPrefix(h, "bitbucket.org") {
				addBitbucketFingerPrint = false
			}
			s.SSHHosts = append(s.SSHHosts, h)
		}
		if addBitbucketFingerPrint {
			s.SSHHosts = append(s.SSHHosts, bitbucketFingerPrint)
		}

		// get config from the runner step
		localStepConfig := localYaml.Runners.Find(c.RunnerName).Steps.Filter(bs.Name)

		// add mount dirs from local config
		for _, m := range localStepConfig.Mounts {
			if err := s.addMount(m); err != nil {
				return nil, err
			}
		}

		// add ssh keys to the step
		if localYaml.SSH.Key != "" {
			s.MountsMain = append(s.MountsMain, fmt.Sprintf("%s:$HOME/.ssh/id_rsa:ro", localYaml.SSH.Key))
			s.MountsPipe = append(s.MountsPipe, fmt.Sprintf("%s:/opt/atlassian/pipelines/agent/ssh/id_rsa_tmp:ro", localYaml.SSH.Key))
		}

		// finally add constant vars
		s.Env["CI"] = "true"
		s.Env["LOCAL_CI"] = "true"
	}

	return steps, nil
}
