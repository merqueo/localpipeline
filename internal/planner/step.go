package planner

import (
	"fmt"
	"io"
	"io/fs"
	"strings"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/remote"
)

type dockerInterface interface {
	FixMounts(mounts []string, repository string, tag string) ([]string, error)
}

type TemplaterInterface interface {
	templateEntrypoint(dest io.WriteCloser, data map[string]interface{}) error
	templateScript(dest io.WriteCloser, data map[string]interface{}) error
	templateAfterscript(dest io.WriteCloser, data map[string]interface{}) error
}

type FilerInterface interface {
	createFile(name string) io.WriteCloser
	MkdirAll(string, fs.FileMode) error
}

type entrypointCommad struct {
	HasLabel2 bool
	label     []string
	script    []string
}

// escapa los caracteres de bash para q no se ejecuten
func (e *entrypointCommad) escape(c []string, sep string) string {
	lbl := strings.Join(c, sep)
	lbl = strings.ReplaceAll(lbl, `\`, `\\\\`)
	lbl = strings.ReplaceAll(lbl, `"`, `\"`)
	return strings.ReplaceAll(lbl, `$`, `\$`)
}

func (e *entrypointCommad) Label() string {
	return e.escape(e.label, " ")
}

func (e *entrypointCommad) Label2() string {
	return e.escape(e.script, " \\\n ")
}

func (e *entrypointCommad) Script() string {
	return strings.Join(e.script, " ")
}

type tmpCommand struct {
	Cmd   []string
	Image string
}

type Service struct {
	ContainerName   string
	ImageRepository string
	ImageTag        string
	Env             []string
	RAM             int64
}

type HostConfig struct {
	WorkDir       config.WorkDir
	StoreCacheDir string
	StoreEntryDir string
	StoreMountDir string
}

func transformCommands(d dockerInterface, scripts []tmpCommand, mCommon []string, mPipes []string, after bool) ([]entrypointCommad, error) {
	cmds := make([]entrypointCommad, len(scripts))
	for i, v := range scripts {
		if v.Image == "" {
			cmds[i] = entrypointCommad{
				label:  v.Cmd,
				script: v.Cmd,
			}
		} else {
			if after {
				v.Cmd = append(v.Cmd, "-e BITBUCKET_EXIT_CODE=\"$BITBUCKET_EXIT_CODE\"")
			}

			image := strings.Replace(v.Image, "atlassian/", "bitbucketpipelines/", 1)
			image = strings.Replace(image, "docker://", "", 1)
			ii := remote.Image(image)

			mounts, err := d.FixMounts(mCommon, ii.Name(), ii.Tag())
			if err != nil {
				return nil, err
			}
			for _, m := range mounts {
				v.Cmd = append(v.Cmd, fmt.Sprintf("-v %s", m))
			}

			mounts, err = d.FixMounts(mPipes, ii.Name(), ii.Tag())
			if err != nil {
				return nil, err
			}
			for _, m := range mounts {
				v.Cmd = append(v.Cmd, fmt.Sprintf("-v %s", m))
			}

			v.Cmd = append(v.Cmd, image)

			cmds[i] = entrypointCommad{
				HasLabel2: true,
				label: []string{
					"task:",
					v.Image,
				},
				script: v.Cmd,
			}
		}
	}
	return cmds, nil
}

func script(s remote.ScriptList, be bitbucketVars) []tmpCommand {

	cl := make([]tmpCommand, len(s))

	for i, v := range s {
		switch vv := v.(type) {
		case string:
			cl[i] = tmpCommand{
				Cmd: []string{vv},
			}
		default:
			x, ok := v.(map[interface{}]interface{})
			if ok {
				pipe, ok := x["pipe"]
				if ok {
					parts := []string{
						"docker container run",
						"--rm -t",
						"-w $(pwd)",
					}
					for y := range be {
						parts = append(parts, fmt.Sprintf("-e %s=\"$%s\"", y, y))
					}
					parts = append(parts, `-e CI="$CI"`, `-e LOCAL_CI="$LOCAL_CI"`)
					vars, ok := x["variables"]
					if ok {
						for y, z := range vars.(map[interface{}]interface{}) {
							parts = append(parts, fmt.Sprintf("-e %s=\"%s\"", y.(string), z.(string)))
						}
					}

					cl[i] = tmpCommand{
						Cmd:   parts,
						Image: pipe.(string),
					}
				}
			}
		}
	}
	return cl
}

type Step struct {
	Prefix          string
	Name            string
	ContainerName   string
	Services        []Service
	ImageRepository string
	ImageTag        string
	Env             load.EnvMap
	RAM             int64
	Script          []tmpCommand
	AfterScript     []tmpCommand
	MountsCommon    []string // shared with pipes
	MountsMain      []string // only for the main container
	MountsPipe      []string // only for pipes
	SSHHosts        []string
	HostConfig      HostConfig
	templater       TemplaterInterface
	filer           FilerInterface
}

func (s *Step) Entrypoint(d dockerInterface) error {
	ff := s.filer.createFile(fmt.Sprintf("%s/%s", s.HostConfig.StoreEntryDir, "entrypoint.sh"))
	defer ff.Close()
	err := s.templater.templateEntrypoint(ff, map[string]interface{}{
		"after": len(s.AfterScript) > 0,
		"name":  s.Name,
	})
	if err != nil {
		return err
	}

	cmds, err := transformCommands(d, s.Script, s.MountsCommon, s.MountsPipe, false)
	if err != nil {
		return err
	}
	ff = s.filer.createFile(fmt.Sprintf("%s/%s", s.HostConfig.StoreEntryDir, "script.sh"))
	defer ff.Close()
	err = s.templater.templateScript(ff, map[string]interface{}{
		"cmds": cmds,
	})
	if err != nil {
		return err
	}

	cmds, err = transformCommands(d, s.AfterScript, s.MountsCommon, s.MountsPipe, true)
	if err != nil {
		return err
	}
	ff = s.filer.createFile(fmt.Sprintf("%s/%s", s.HostConfig.StoreEntryDir, "after-script.sh"))
	defer ff.Close()
	err = s.templater.templateAfterscript(ff, map[string]interface{}{
		"cmds": cmds,
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Step) KnownHosts() error {
	local := fmt.Sprintf("%s/%s", s.HostConfig.StoreEntryDir, "known_hosts")
	ff := s.filer.createFile(local)
	defer ff.Close()

	for _, h := range s.SSHHosts {
		_, err := ff.Write([]byte(h))
		if err != nil {
			return err
		}
	}

	s.MountsMain = append(s.MountsMain, fmt.Sprintf("%s:%s", local, "$HOME/.ssh/known_hosts"))
	s.MountsPipe = append(s.MountsPipe, fmt.Sprintf("%s:%s", local, "/opt/atlassian/pipelines/agent/ssh/known_hosts"))

	return nil
}

func (s *Step) addMount(m string) error {
	if strings.HasPrefix(m, "/") {
		return fmt.Errorf("invalid mount origin: %s", m)
	} else if strings.Contains(m, ":") {
		m = fmt.Sprintf("%s/%s", s.HostConfig.StoreMountDir, m)
	} else {
		err := s.filer.MkdirAll(fmt.Sprintf("%s/%s", s.HostConfig.WorkDir, m), 0755)
		if err != nil {
			return err
		}
		m = fmt.Sprintf("%s/%s:%s/%s", s.HostConfig.StoreMountDir, m, s.Env[BitbucketCloneDir], m)
	}

	parts := strings.Split(m, ":")
	err := s.filer.MkdirAll(parts[0], 0755)
	if err != nil {
		return err
	}
	s.MountsCommon = append(s.MountsCommon, m)
	return nil
}

func (s *Step) addCaches(caches []string, definitions map[string]string) error {
	for _, c := range caches {
		var dst string
		switch c {
		case "docker":
			// TODO build
			continue
		case "composer":
			dst = "$HOME/.composer/cache"
		case "dotnetcore":
			// TODO build
			continue
		case "gradle":
			// TODO build
			continue
		case "ivy2":
			// TODO build
			continue
		case "maven":
			// TODO build
			continue
		case "node":
			// TODO build
			continue
		case "pip":
			// TODO build
			continue
		case "stb":
			// TODO build
			continue
		default:
			d, ok := definitions[c]
			if !ok {
				return fmt.Errorf("no config definition for: %s", c)
			}
			dst = d
		}

		src := fmt.Sprintf("%s/%s", s.HostConfig.StoreCacheDir, c)
		err := s.filer.MkdirAll(src, 0755)
		if err != nil {
			return fmt.Errorf("cache %s: %w", c, err)
		}

		s.MountsCommon = append(s.MountsCommon, fmt.Sprintf("%s:%s", src, dst))
	}

	return nil
}

func (s *Step) addServices(services []string, def remote.Services, size string) error {
	var usedRAM, assignedRAM int64
	for _, service := range services {
		definition, ok := def[service]
		if !ok {
			return fmt.Errorf("service: %s is not defined", service)
		}
		if definition.Memory == 0 {
			definition.Memory = 1204
		} else {
			if definition.Memory < 128 {
				return fmt.Errorf("service: %s memory can't be lower than 128", service)
			}
			switch size {
			case "2x":
				if definition.Memory > 7128 {
					return fmt.Errorf("service: %s memory can't be greater than 7218", service)
				}
			default:
				if definition.Memory > 3072 {
					return fmt.Errorf("service: %s memory can't be greater than 3072", service)
				}
			}
		}

		ram := definition.Memory * 1024 * 1024

		ss := Service{
			ContainerName:   fmt.Sprintf("%s-service-%s", s.Prefix, service),
			ImageRepository: definition.Image.Name(),
			ImageTag:        definition.Image.Tag(),
			Env:             definition.Envs.ToArray(),
			RAM:             ram,
		}

		s.Services = append(s.Services, ss)

		usedRAM += ram
	}

	switch size {
	case "2x":
		assignedRAM = 8 * 1024 * 1024 * 1024
	default:
		assignedRAM = 4 * 1024 * 1024 * 1024
	}

	if usedRAM > assignedRAM {
		return fmt.Errorf("memory error: sum of services memory %d is greaten than %d", usedRAM, assignedRAM)
	}

	s.RAM = assignedRAM - usedRAM

	return nil
}

type StepBuilder struct {
	Config    *config.Config
	BBVars    bitbucketVars
	Filer     FilerInterface
	Templater TemplaterInterface
}

func (b *StepBuilder) New(bs *remote.Step) (*Step, error) {
	hc := HostConfig{
		WorkDir:       b.Config.WorkDir,
		StoreCacheDir: fmt.Sprintf("%s/cache", b.Config.StoreDir),
		StoreEntryDir: fmt.Sprintf("%s/%s/%s/entry", b.Config.StoreDir, b.Config.RunnerName, bs.Name),
		StoreMountDir: fmt.Sprintf("%s/%s/%s/mount", b.Config.StoreDir, b.Config.RunnerName, bs.Name),
	}

	prefix := fmt.Sprintf("localpipeline-%s-%s", b.Config.Name, b.Config.RunnerName)

	envs := load.EnvMap{}
	envs.Add(b.BBVars)

	err := b.Filer.MkdirAll(hc.StoreEntryDir, 0755)
	if err != nil {
		return nil, err
	}

	return &Step{
		Prefix:          prefix,
		Name:            bs.Name,
		ContainerName:   fmt.Sprintf("%s-step", prefix),
		Services:        make([]Service, 0),
		ImageRepository: bs.Image.Name(),
		ImageTag:        bs.Image.Tag(),
		Env:             envs,
		MountsCommon: []string{
			// mount docker
			"/usr/bin/docker:/usr/bin/docker",
			// mount source
			fmt.Sprintf("%s:%s", b.Config.WorkDir, b.BBVars[BitbucketCloneDir]),
		},
		MountsMain: []string{
			// mount entrypoint
			fmt.Sprintf("%s:/localpipeline/entrypoint", hc.StoreEntryDir),
		},
		MountsPipe:  []string{},
		SSHHosts:    []string{},
		Script:      script(bs.Scripts, b.BBVars),
		AfterScript: script(bs.AfterScripts, b.BBVars),
		HostConfig:  hc,
		templater:   b.Templater,
		filer:       b.Filer,
	}, nil
}
