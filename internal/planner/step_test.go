package planner

import (
	"errors"
	"io"
	"io/fs"
	"reflect"
	"testing"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/remote"
	"github.com/d4l3k/messagediff"
	"github.com/ory/dockertest/v3/docker/pkg/ioutils"
	"github.com/stretchr/testify/mock"
)

type CloserMock struct {
	mock.Mock
}

func (m *CloserMock) Close() error {
	return m.Called().Error(0)
}

type WriteCloserMock struct {
	*CloserMock
}

func (m *WriteCloserMock) Write(p []byte) (n int, err error) {
	args := m.Called(p)
	return args.Int(0), args.Error(1)
}

type DockerInterfaceMock struct {
	mock.Mock
}

func (m *DockerInterfaceMock) FixMounts(mounts []string, repository string, tag string) ([]string, error) {
	args := m.Called(mounts, repository, tag)
	return args.Get(0).([]string), args.Error(1)
}

type FilerInterfaceMock struct {
	mock.Mock
}

func (m *FilerInterfaceMock) createFile(name string) io.WriteCloser {
	args := m.Called(name)
	return args.Get(0).(io.WriteCloser)
}

func (m *FilerInterfaceMock) MkdirAll(name string, perm fs.FileMode) error {
	args := m.Called(name, perm)
	return args.Error(0)
}

type TemplaterInterfaceMock struct {
	mock.Mock
}

func (m *TemplaterInterfaceMock) templateEntrypoint(dest io.WriteCloser, data map[string]interface{}) error {
	args := m.Called(dest, data)
	return args.Error(0)
}

func (m *TemplaterInterfaceMock) templateScript(dest io.WriteCloser, data map[string]interface{}) error {
	args := m.Called(dest, data)
	return args.Error(0)
}

func (m *TemplaterInterfaceMock) templateAfterscript(dest io.WriteCloser, data map[string]interface{}) error {
	args := m.Called(dest, data)
	return args.Error(0)
}

func Test_transformCommands(t *testing.T) {
	type args struct {
		d       *DockerInterfaceMock
		scripts []tmpCommand
		mCommon []string
		mPipes  []string
		after   bool
	}
	tests := []struct {
		name    string
		args    args
		mocker  func(*DockerInterfaceMock)
		want    []entrypointCommad
		wantErr bool
	}{
		{
			name: "basic commands",
			args: args{
				d: &DockerInterfaceMock{},
				scripts: []tmpCommand{
					{
						Cmd: []string{"env"},
					},
					{
						Cmd: []string{`echo "hola \"$mundo\""`},
					},
				},
				after: false,
			},
			mocker: func(d *DockerInterfaceMock) {},
			want: []entrypointCommad{
				{
					label:  []string{`env`},
					script: []string{`env`},
				},
				{
					label:  []string{`echo "hola \"$mundo\""`},
					script: []string{`echo "hola \"$mundo\""`},
				},
			},
			wantErr: false,
		},
		{
			name: "pipe in linux with after-script",
			args: args{
				d: &DockerInterfaceMock{},
				scripts: []tmpCommand{
					{
						Cmd:   []string{},
						Image: `atlassian/xxxxx:1.0.0`,
					},
				},
				mCommon: []string{
					"/home/juan:/home/juan",
				},
				mPipes: []string{
					"/home/pipes:/home/pipes",
				},
				after: true,
			},
			mocker: func(d *DockerInterfaceMock) {
				d.On(
					"FixMounts",
					[]string{"/home/juan:/home/juan"},
					"bitbucketpipelines/xxxxx",
					"1.0.0",
				).Return(
					[]string{"/home/juan:/home/juan"},
					nil,
				).Once()
				d.On(
					"FixMounts",
					[]string{"/home/pipes:/home/pipes"},
					"bitbucketpipelines/xxxxx",
					"1.0.0",
				).Return(
					[]string{"/home/pipes:/home/pipes"},
					nil,
				).Once()
			},
			want: []entrypointCommad{
				{
					HasLabel2: true,
					label: []string{
						"task:",
						"atlassian/xxxxx:1.0.0",
					},
					script: []string{
						"-e BITBUCKET_EXIT_CODE=\"$BITBUCKET_EXIT_CODE\"",
						"-v /home/juan:/home/juan",
						"-v /home/pipes:/home/pipes",
						"bitbucketpipelines/xxxxx:1.0.0",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "pipe in mac without after-script",
			args: args{
				d: &DockerInterfaceMock{},
				scripts: []tmpCommand{
					{
						Cmd:   []string{},
						Image: `docker://merqueobuilders/xxxxx:1.0.0`,
					},
				},
				mCommon: []string{
					"/home/juan:/home/juan",
				},
				mPipes: []string{
					"/home/pipes:/home/pipes",
				},
				after: false,
			},
			mocker: func(d *DockerInterfaceMock) {
				d.On(
					"FixMounts",
					[]string{"/home/juan:/home/juan"},
					"merqueobuilders/xxxxx",
					"1.0.0",
				).Return(
					[]string{"/home/juan:/home/juan"},
					nil,
				).Once()
				d.On("FixMounts",
					[]string{"/home/pipes:/home/pipes"},
					"merqueobuilders/xxxxx",
					"1.0.0",
				).Return(
					[]string{"/home/pipes:/home/pipes"},
					nil,
				).Once()
			},
			want: []entrypointCommad{
				{
					HasLabel2: true,
					label: []string{
						"task:",
						"docker://merqueobuilders/xxxxx:1.0.0",
					},
					script: []string{
						"-v /home/juan:/home/juan",
						"-v /home/pipes:/home/pipes",
						"merqueobuilders/xxxxx:1.0.0",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "pipe with error 1",
			args: args{
				d: &DockerInterfaceMock{},
				scripts: []tmpCommand{
					{
						Cmd:   []string{},
						Image: `atlassian/yyyy:1.0.0`,
					},
				},
				mCommon: []string{
					"/home/juan:/home/juan",
				},
				mPipes: []string{
					"/home/pipes:/home/pipes",
				},
				after: true,
			},
			mocker: func(d *DockerInterfaceMock) {
				d.On("FixMounts",
					[]string{"/home/juan:/home/juan"},
					"bitbucketpipelines/yyyy",
					"1.0.0",
				).Return(
					[]string{},
					errors.New("ups"),
				).Once()
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "pipe with error 2",
			args: args{
				d: &DockerInterfaceMock{},
				scripts: []tmpCommand{
					{
						Cmd:   []string{},
						Image: `atlassian/yyyy:1.0.0`,
					},
				},
				mCommon: []string{
					"/home/juan:/home/juan",
				},
				mPipes: []string{
					"/home/pipes:/home/pipes",
				},
				after: true,
			},
			mocker: func(d *DockerInterfaceMock) {
				d.On(
					"FixMounts",
					[]string{"/home/juan:/home/juan"},
					"bitbucketpipelines/yyyy",
					"1.0.0",
				).Return(
					[]string{},
					nil,
				).Once()
				d.On(
					"FixMounts",
					[]string{"/home/pipes:/home/pipes"},
					"bitbucketpipelines/yyyy",
					"1.0.0",
				).Return(
					[]string{},
					errors.New("ups"),
				).Once()
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mocker(tt.args.d)
			got, err := transformCommands(tt.args.d, tt.args.scripts, tt.args.mCommon, tt.args.mPipes, tt.args.after)
			if (err != nil) != tt.wantErr {
				t.Errorf("Step.transformCommands() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if diff, equal := messagediff.PrettyDiff(tt.want, got); !equal {
				t.Errorf("Step.transformCommands() = %#v\n%s", got, diff)
			}

			tt.args.d.AssertExpectations(t)
		})
	}
}

func Test_script(t *testing.T) {
	type args struct {
		s  remote.ScriptList
		be map[string]string
	}
	tests := []struct {
		name string
		args args
		want []tmpCommand
	}{
		{
			name: "basic",
			args: args{
				s: remote.ScriptList{
					"env",
					map[interface{}]interface{}{
						"pipe": "docker://abc:1.0.0",
						"variables": map[interface{}]interface{}{
							"AAA": "BBB",
						},
					},
				},
				be: map[string]string{
					"XXX": "YYY",
				},
			},
			want: []tmpCommand{
				{
					Cmd: []string{"env"},
				},
				{
					Image: "docker://abc:1.0.0",
					Cmd: []string{
						`docker container run`,
						`--rm -t`,
						`-w $(pwd)`,
						`-e XXX="$XXX"`,
						`-e CI="$CI"`,
						`-e LOCAL_CI="$LOCAL_CI"`,
						`-e AAA="BBB"`,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := script(tt.args.s, tt.args.be); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("script() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStep_Entrypoint(t *testing.T) {
	type fields struct {
		Name         string
		Script       []tmpCommand
		AfterScript  []tmpCommand
		MountsCommon []string
		MountsPipes  []string
		HostConfig   HostConfig
		filer        *FilerInterfaceMock
		templater    *TemplaterInterfaceMock
		closer       *CloserMock
	}
	type args struct {
		d *DockerInterfaceMock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mocker  func(fields, args)
		wantErr bool
	}{
		{
			name: "complete test",
			fields: fields{
				Name: "step1",
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				MountsCommon: []string{
					"vendor",
				},
				MountsPipes: []string{
					"/a:/b",
				},
				Script: []tmpCommand{
					{
						Image: "docker://abc:1.0.0",
					},
				},
				AfterScript: []tmpCommand{
					{
						Image: "docker://xyz:1.0.0",
					},
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(3)

				a.d.On("FixMounts", []string{"vendor"}, "abc", "1.0.0").Return([]string{"/home/abc/vendor"}, nil).Once()
				a.d.On("FixMounts", []string{"vendor"}, "xyz", "1.0.0").Return([]string{"/home/xyz/vendor"}, nil).Once()
				a.d.On("FixMounts", []string{"/a:/b"}, "abc", "1.0.0").Return([]string{"/a:/b"}, nil).Once()
				a.d.On("FixMounts", []string{"/a:/b"}, "xyz", "1.0.0").Return([]string{"/a:/b"}, nil).Once()

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": true,
					"name":  "step1",
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/script.sh").Return(wc).Once()
				f.templater.On("templateScript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{
						{
							HasLabel2: true,
							label: []string{
								"task:",
								"docker://abc:1.0.0",
							},
							script: []string{
								"-v /home/abc/vendor",
								"-v /a:/b",
								"abc:1.0.0",
							},
						},
					},
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/after-script.sh").Return(wc).Once()
				f.templater.On("templateAfterscript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{
						{
							HasLabel2: true,
							label: []string{
								"task:",
								"docker://xyz:1.0.0",
							},
							script: []string{
								"-e BITBUCKET_EXIT_CODE=\"$BITBUCKET_EXIT_CODE\"",
								"-v /home/xyz/vendor",
								"-v /a:/b",
								"xyz:1.0.0",
							},
						},
					},
				}).Return(nil).Once()
			},
			wantErr: false,
		},
		{
			name: "basic test",
			fields: fields{
				Name: "step1",
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(3)

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": false,
					"name":  "step1",
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/script.sh").Return(wc).Once()
				f.templater.On("templateScript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/after-script.sh").Return(wc).Once()
				f.templater.On("templateAfterscript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(nil).Once()
			},
			wantErr: false,
		},
		{
			name: "fail template entrypoint",
			fields: fields{
				Name: "step1",
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Once()

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": false,
					"name":  "step1",
				}).Return(errors.New("ups")).Once()
			},
			wantErr: true,
		},
		{
			name: "fail template script",
			fields: fields{
				Name: "step1",
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(2)

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": false,
					"name":  "step1",
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/script.sh").Return(wc).Once()
				f.templater.On("templateScript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(errors.New("ups")).Once()
			},
			wantErr: true,
		},
		{
			name: "fail template after-script",
			fields: fields{
				Name: "step1",
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(3)

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": false,
					"name":  "step1",
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/script.sh").Return(wc).Once()
				f.templater.On("templateScript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/after-script.sh").Return(wc).Once()
				f.templater.On("templateAfterscript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(errors.New("ups")).Once()
			},
			wantErr: true,
		},
		{
			name: "fail template script, command fail",
			fields: fields{
				Name:         "step1",
				MountsCommon: []string{},
				MountsPipes:  []string{},
				Script: []tmpCommand{
					{
						Image: "docker://xxxx:0.0.0",
					},
				},
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(1)

				a.d.On("FixMounts", []string{}, "xxxx", "0.0.0").Return([]string{}, errors.New("ups")).Once()

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": false,
					"name":  "step1",
				}).Return(nil).Once()
			},
			wantErr: true,
		},
		{
			name: "fail template after-script, command fail",
			fields: fields{
				Name:         "step1",
				MountsCommon: []string{},
				MountsPipes:  []string{},
				AfterScript: []tmpCommand{
					{
						Image: "docker://xxxx:0.0.1",
					},
				},
				HostConfig: HostConfig{
					StoreEntryDir: "/home/juan/.cache",
				},
				filer:     &FilerInterfaceMock{},
				templater: &TemplaterInterfaceMock{},
				closer:    &CloserMock{},
			},
			args: args{
				d: &DockerInterfaceMock{},
			},
			mocker: func(f fields, a args) {

				f.closer.On("Close").Return(nil).Times(2)

				a.d.On("FixMounts", []string{}, "xxxx", "0.0.1").Return([]string{}, errors.New("ups")).Once()

				wc := ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/entrypoint.sh").Return(wc).Once()
				f.templater.On("templateEntrypoint", wc, map[string]interface{}{
					"after": true,
					"name":  "step1",
				}).Return(nil).Once()

				wc = ioutils.NewWriteCloserWrapper(nil, f.closer.Close)
				f.filer.On("createFile", "/home/juan/.cache/script.sh").Return(wc).Once()
				f.templater.On("templateScript", wc, map[string]interface{}{
					"cmds": []entrypointCommad{},
				}).Return(nil).Once()
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Step{
				Name:         tt.fields.Name,
				Script:       tt.fields.Script,
				AfterScript:  tt.fields.AfterScript,
				MountsCommon: tt.fields.MountsCommon,
				MountsPipe:   tt.fields.MountsPipes,
				HostConfig:   tt.fields.HostConfig,
				filer:        tt.fields.filer,
				templater:    tt.fields.templater,
			}
			tt.mocker(tt.fields, tt.args)
			if err := s.Entrypoint(tt.args.d); (err != nil) != tt.wantErr {
				t.Errorf("Step.Entrypoint() error = %v, wantErr %v", err, tt.wantErr)
			}

			tt.args.d.AssertExpectations(t)
			tt.fields.filer.AssertExpectations(t)
			tt.fields.templater.AssertExpectations(t)
			tt.fields.closer.AssertExpectations(t)
		})
	}
}

func TestStep_AddMount(t *testing.T) {
	type fields struct {
		Env        load.EnvMap
		HostConfig HostConfig
		filer      *FilerInterfaceMock
	}
	type args struct {
		m string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mocker  func(fields)
		want    *Step
		wantErr bool
	}{
		{
			name: "simple mount",
			fields: fields{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				Env: load.EnvMap{
					BitbucketCloneDir: "/opt/project",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				"vendor",
			},
			mocker: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/work/vendor", fs.FileMode(0755)).Return(nil).Once()
				f.filer.On("MkdirAll", "/home/juan/.cache/vendor", fs.FileMode(0755)).Return(nil).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				Env: load.EnvMap{
					BitbucketCloneDir: "/opt/project",
				},
				MountsCommon: []string{
					"/home/juan/.cache/vendor:/opt/project/vendor",
				},
			},
			wantErr: false,
		},
		{
			name: "simple mount, fail mkdir in workdir",
			fields: fields{
				HostConfig: HostConfig{
					WorkDir: "/home/juan/work",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				"vendor",
			},
			mocker: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/work/vendor", fs.FileMode(0755)).Return(errors.New("ups")).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					WorkDir: "/home/juan/work",
				},
				MountsCommon: []string{},
			},
			wantErr: true,
		},
		{
			name: "simple mount, fail mkdir in host mount",
			fields: fields{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				Env: load.EnvMap{
					BitbucketCloneDir: "/opt/project",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				"vendor",
			},
			mocker: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/work/vendor", fs.FileMode(0755)).Return(nil).Once()
				f.filer.On("MkdirAll", "/home/juan/.cache/vendor", fs.FileMode(0755)).Return(errors.New("ups")).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				Env: load.EnvMap{
					BitbucketCloneDir: "/opt/project",
				},
				MountsCommon: []string{},
			},
			wantErr: true,
		},
		{
			name: "mount in specific folder",
			fields: fields{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				"vendor:/opt/project/vendor",
			},
			mocker: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/.cache/vendor", fs.FileMode(0755)).Return(nil).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreMountDir: "/home/juan/.cache",
					WorkDir:       "/home/juan/work",
				},
				MountsCommon: []string{
					"/home/juan/.cache/vendor:/opt/project/vendor",
				},
			},
			wantErr: false,
		},
		{
			name: "invalid mount",
			fields: fields{
				filer: &FilerInterfaceMock{},
			},
			args: args{
				m: "/home:/home",
			},
			mocker: func(f fields) {},
			want: &Step{
				MountsCommon: []string{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Step{
				Env:          tt.fields.Env,
				MountsCommon: []string{},
				HostConfig:   tt.fields.HostConfig,
				filer:        tt.fields.filer,
			}

			tt.mocker(tt.fields)

			if err := s.addMount(tt.args.m); (err != nil) != tt.wantErr {
				t.Errorf("Step.AddMount() error = %v, wantErr %v", err, tt.wantErr)
			}

			tt.want.filer = tt.fields.filer

			if !reflect.DeepEqual(s, tt.want) {
				t.Errorf("Step = %v, want %v", s, tt.want)
			}

			tt.fields.filer.AssertExpectations(t)
		})

	}
}

func TestStep_addCaches(t *testing.T) {
	type fields struct {
		HostConfig HostConfig
		filer      *FilerInterfaceMock
	}
	type args struct {
		caches      []string
		definitions map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mock    func(fields)
		want    *Step
		wantErr bool
	}{
		{
			name: "basic",
			fields: fields{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				caches: []string{
					"docker",
					"composer",
					"dotnetcore",
					"gradle",
					"ivy2",
					"maven",
					"node",
					"pip",
					"stb",
					"xyz",
				},
				definitions: map[string]string{
					"xyz": "~/.xxx/downloads",
				},
			},
			mock: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/.cache/composer", fs.FileMode(0755)).Return(nil).Once()
				f.filer.On("MkdirAll", "/home/juan/.cache/xyz", fs.FileMode(0755)).Return(nil).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				MountsCommon: []string{
					"/home/juan/.cache/composer:$HOME/.composer/cache",
					"/home/juan/.cache/xyz:~/.xxx/downloads",
				},
			},
			wantErr: false,
		},
		{
			name: "with out definition error",
			fields: fields{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				caches: []string{
					"xyz",
				},
				definitions: map[string]string{},
			},
			mock: func(f fields) {},
			want: &Step{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				MountsCommon: []string{},
			},
			wantErr: true,
		},
		{
			name: "mount point creation error",
			fields: fields{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				filer: &FilerInterfaceMock{},
			},
			args: args{
				caches: []string{
					"xyz",
				},
				definitions: map[string]string{
					"xyz": "~/.xxx/downloads",
				},
			},
			mock: func(f fields) {
				f.filer.On("MkdirAll", "/home/juan/.cache/xyz", fs.FileMode(0755)).Return(errors.New("ups")).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreCacheDir: "/home/juan/.cache",
				},
				MountsCommon: []string{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Step{
				MountsCommon: []string{},
				HostConfig:   tt.fields.HostConfig,
				filer:        tt.fields.filer,
			}
			tt.mock(tt.fields)
			if err := s.addCaches(tt.args.caches, tt.args.definitions); (err != nil) != tt.wantErr {
				t.Errorf("Step.addCaches() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.want.filer = tt.fields.filer
			if !reflect.DeepEqual(s, tt.want) {
				t.Errorf("Step = %v, want %v", s, tt.want)
			}
			tt.fields.filer.AssertExpectations(t)
		})
	}
}

func TestStep_addServices(t *testing.T) {
	type fields struct {
		Prefix   string
		Services []Service
	}
	type args struct {
		services []string
		def      remote.Services
		size     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Step
		wantErr bool
	}{
		{
			name: "without services 1x",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{},
			want: &Step{
				Prefix:   "step-1",
				Services: []Service{},
				RAM:      4294967296,
			},
			wantErr: false,
		},
		{
			name: "without services 2x",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				size: "2x",
			},
			want: &Step{
				Prefix:   "step-1",
				Services: []Service{},
				RAM:      8589934592,
			},
			wantErr: false,
		},
		{
			name: "one service",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
				},
				def: remote.Services{
					"mysql": remote.Service{
						Image:  "mysql:5.7",
						Memory: 512,
						Envs: load.EnvMap{
							"XXX": "YYY",
						},
					},
				},
				size: "1x",
			},
			want: &Step{
				Prefix: "step-1",
				Services: []Service{
					{
						ContainerName:   "step-1-service-mysql",
						ImageRepository: "mysql",
						ImageTag:        "5.7",
						Env: []string{
							"XXX=YYY",
						},
						RAM: 536870912,
					},
				},
				RAM: 3758096384,
			},
			wantErr: false,
		},
		{
			name: "service ram lower than 128",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
				},
				def: remote.Services{
					"mysql": remote.Service{
						Image:  "mysql:5.7",
						Memory: 64,
						Envs: load.EnvMap{
							"XXX": "YYY",
						},
					},
				},
				size: "1x",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad service ram error 1x",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
				},
				def: remote.Services{
					"mysql": remote.Service{
						Image:  "mysql:5.7",
						Memory: 4000,
						Envs: load.EnvMap{
							"XXX": "YYY",
						},
					},
				},
				size: "1x",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad service ram error 2x",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
				},
				def: remote.Services{
					"mysql": remote.Service{
						Image:  "mysql:5.7",
						Memory: 8000,
						Envs: load.EnvMap{
							"XXX": "YYY",
						},
					},
				},
				size: "2x",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad sum ram",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
					"redis",
				},
				def: remote.Services{
					"mysql": remote.Service{
						Image:  "mysql:5.7",
						Memory: 3072,
						Envs: load.EnvMap{
							"XXX": "YYY",
						},
					},
					"redis": remote.Service{
						Image:  "redis:5",
						Memory: 3072,
						Envs: load.EnvMap{
							"AAA": "BBB",
						},
					},
				},
				size: "1x",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "without definition  error",
			fields: fields{
				Prefix:   "step-1",
				Services: []Service{},
			},
			args: args{
				services: []string{
					"mysql",
				},
				size: "1x",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Step{
				Prefix:   tt.fields.Prefix,
				Services: tt.fields.Services,
			}
			if err := s.addServices(tt.args.services, tt.args.def, tt.args.size); (err != nil) != tt.wantErr {
				t.Errorf("Step.addServices() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !tt.wantErr && !reflect.DeepEqual(s, tt.want) {
				t.Errorf("Step = %v, want %v", s, tt.want)
			}
		})
	}
}

func Test_newStep(t *testing.T) {
	type args struct {
		be map[string]string
		c  *config.Config
		bs *remote.Step
		fi *FilerInterfaceMock
	}
	tests := []struct {
		name    string
		args    args
		mock    func(args)
		want    *Step
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				be: map[string]string{
					BitbucketCloneDir: "/opt/build",
				},
				c: &config.Config{
					Name:       "test.1",
					WorkDir:    "/home/xyz/src",
					StoreDir:   "/home/xyz/.cache",
					RunnerName: "r1",
				},
				bs: &remote.Step{
					Name:  "s1",
					Image: "abc/xyz:1.0.1",
					Scripts: remote.ScriptList{
						"hola",
					},
					AfterScripts: remote.ScriptList{
						"mundo",
					},
				},
				fi: &FilerInterfaceMock{},
			},
			mock: func(a args) {
				a.fi.On("MkdirAll", "/home/xyz/.cache/r1/s1/entry", fs.FileMode(0755)).Return(nil).Once()
			},
			want: &Step{
				Prefix:          "localpipeline-test.1-r1",
				Name:            "s1",
				ContainerName:   "localpipeline-test.1-r1-step",
				Services:        []Service{},
				ImageRepository: "abc/xyz",
				ImageTag:        "1.0.1",
				Env: load.EnvMap{
					BitbucketCloneDir: "/opt/build",
				},
				RAM: 0,
				Script: []tmpCommand{
					{
						Cmd: []string{"hola"},
					},
				},
				AfterScript: []tmpCommand{
					{
						Cmd: []string{"mundo"},
					},
				},
				MountsCommon: []string{
					"/usr/bin/docker:/usr/bin/docker",
					"/home/xyz/src:/opt/build",
				},
				MountsMain: []string{
					"/home/xyz/.cache/r1/s1/entry:/localpipeline/entrypoint",
				},
				MountsPipe: []string{},
				SSHHosts:   []string{},
				HostConfig: HostConfig{
					WorkDir:       "/home/xyz/src",
					StoreCacheDir: "/home/xyz/.cache/cache",
					StoreEntryDir: "/home/xyz/.cache/r1/s1/entry",
					StoreMountDir: "/home/xyz/.cache/r1/s1/mount",
				},
			},
			wantErr: false,
		},
		{
			name: "fail mkdir",
			args: args{
				be: map[string]string{
					BitbucketCloneDir: "/opt/build",
				},
				c: &config.Config{
					Name:       "test.1",
					WorkDir:    "/home/xyz/src",
					StoreDir:   "/home/xyz/.cache",
					RunnerName: "r1",
				},
				bs: &remote.Step{
					Name:  "s1",
					Image: "abc/xyz:1.0.1",
					Scripts: remote.ScriptList{
						"hola",
					},
					AfterScripts: remote.ScriptList{
						"mundo",
					},
				},
				fi: &FilerInterfaceMock{},
			},
			mock: func(a args) {
				a.fi.On("MkdirAll", "/home/xyz/.cache/r1/s1/entry", fs.FileMode(0755)).Return(errors.New("ups")).Once()
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			te := &TemplaterInterfaceMock{}

			tt.mock(tt.args)

			builder := StepBuilder{
				Config:    tt.args.c,
				BBVars:    tt.args.be,
				Templater: te,
				Filer:     tt.args.fi,
			}

			got, err := builder.New(tt.args.bs)
			if (err != nil) != tt.wantErr {
				t.Errorf("newStep() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				tt.want.templater = te
				tt.want.filer = tt.args.fi
			}
			if diff, equal := messagediff.PrettyDiff(tt.want, got); !equal {
				t.Errorf("Something() = %#v\n%s", got, diff)
			}

			te.AssertExpectations(t)
			tt.args.fi.AssertExpectations(t)
		})
	}
}

func TestStep_KnownHosts(t *testing.T) {
	type fields struct {
		MountsMain  []string
		MountsPipe  []string
		SSHHosts    []string
		HostConfig  HostConfig
		filer       *FilerInterfaceMock
		writecloser *WriteCloserMock
	}
	tests := []struct {
		name    string
		fields  fields
		mocker  func(fields)
		want    *Step
		wantErr bool
	}{
		{
			name: "ok",
			fields: fields{
				filer: &FilerInterfaceMock{},
				writecloser: &WriteCloserMock{
					CloserMock: &CloserMock{},
				},
				MountsMain: []string{},
				MountsPipe: []string{},
				SSHHosts: []string{
					bitbucketFingerPrint,
				},
				HostConfig: HostConfig{
					StoreEntryDir: "/x/y/z",
				},
			},
			mocker: func(f fields) {
				f.writecloser.On("Write", []uint8(bitbucketFingerPrint)).Return(0, nil).Once()
				f.writecloser.On("Close").Return(nil).Once()
				f.filer.On("createFile", "/x/y/z/known_hosts").Return(f.writecloser).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreEntryDir: "/x/y/z",
				},
				MountsMain: []string{
					"/x/y/z/known_hosts:$HOME/.ssh/known_hosts",
				},
				MountsPipe: []string{
					"/x/y/z/known_hosts:/opt/atlassian/pipelines/agent/ssh/known_hosts",
				},
				SSHHosts: []string{
					bitbucketFingerPrint,
				},
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				filer: &FilerInterfaceMock{},
				writecloser: &WriteCloserMock{
					CloserMock: &CloserMock{},
				},
				MountsMain: []string{},
				MountsPipe: []string{},
				SSHHosts: []string{
					bitbucketFingerPrint,
				},
				HostConfig: HostConfig{
					StoreEntryDir: "/x/y/z",
				},
			},
			mocker: func(f fields) {
				f.writecloser.On("Write", []uint8(bitbucketFingerPrint)).Return(0, errors.New("ups")).Once()
				f.writecloser.On("Close").Return(nil).Once()
				f.filer.On("createFile", "/x/y/z/known_hosts").Return(f.writecloser).Once()
			},
			want: &Step{
				HostConfig: HostConfig{
					StoreEntryDir: "/x/y/z",
				},
				MountsMain: []string{},
				MountsPipe: []string{},
				SSHHosts: []string{
					bitbucketFingerPrint,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mocker(tt.fields)
			s := &Step{
				MountsMain: tt.fields.MountsMain,
				MountsPipe: tt.fields.MountsPipe,
				SSHHosts:   tt.fields.SSHHosts,
				HostConfig: tt.fields.HostConfig,
				filer:      tt.fields.filer,
			}
			if err := s.KnownHosts(); (err != nil) != tt.wantErr {
				t.Errorf("Step.KnownHosts() error = %v, wantErr %v", err, tt.wantErr)
			}

			tt.want.filer = tt.fields.filer
			if diff, equal := messagediff.PrettyDiff(tt.want, s); !equal {
				t.Errorf("Something() = %#v\n%s", s, diff)
			}

			tt.fields.filer.AssertExpectations(t)
			tt.fields.writecloser.AssertExpectations(t)
		})
	}
}

func Test_entrypointCommad_Label(t *testing.T) {
	type fields struct {
		label  []string
		script []string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "test 1",
			fields: fields{
				label: []string{"cmd", "-version", "$PATH"},
			},
			want: `cmd -version \$PATH`,
		},
		{
			name: "test 2",
			fields: fields{
				label: []string{`\`},
			},
			want: `\\\\`,
		},
		{
			name: "test 3",
			fields: fields{
				label: []string{`$`},
			},
			want: `\$`,
		},
		{
			name: "test 4",
			fields: fields{
				label: []string{`""`},
			},
			want: `\"\"`,
		},
		{
			name: "test 5",
			fields: fields{
				label: []string{`\n`},
			},
			want: `\\\\n`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &entrypointCommad{
				label:  tt.fields.label,
				script: tt.fields.script,
			}
			if got := e.Label(); got != tt.want {
				t.Errorf("entrypointCommad.Label() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_entrypointCommad_Label2(t *testing.T) {
	type fields struct {
		script []string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "test 1",
			fields: fields{
				script: []string{"cmd", "-version", `$PATH="xx"`},
			},
			want: "cmd \\\\\\\\\n -version \\\\\\\\\n \\$PATH=\\\"xx\\\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &entrypointCommad{
				script: tt.fields.script,
			}
			if got := e.Label2(); got != tt.want {
				t.Errorf("entrypointCommad.Label2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_entrypointCommad_Script(t *testing.T) {
	type fields struct {
		script []string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "test 1",
			fields: fields{
				script: []string{"xx", "$yy", `"xx"`, `\-`},
			},
			want: `xx $yy "xx" \-`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &entrypointCommad{
				script: tt.fields.script,
			}
			if got := e.Script(); got != tt.want {
				t.Errorf("entrypointCommad.Script() = %v, want %v", got, tt.want)
			}
		})
	}
}
