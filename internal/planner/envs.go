package planner

import (
	"fmt"
	"os"
	"strings"
	"time"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"github.com/go-git/go-git/v5"
)

// BitbucketCloneDir name used to get the container workdir
const BitbucketCloneDir = "BITBUCKET_CLONE_DIR"

type bitbucketVars map[string]string

func getBitbucketCommit(w config.WorkDir) string {
	bc := os.Getenv("BITBUCKET_COMMIT")
	if bc != "" {
		return bc
	}

	r, err := git.PlainOpen(string(w))
	if err != nil {
		panic(err)
	}

	h, err := r.Head()
	if err != nil {
		panic(err)
	}
	return strings.TrimPrefix(string(h.Name()), "refs/heads/")
}

func getBitbucketBuildNumber() string {
	bbn := os.Getenv("BITBUCKET_BUILD_NUMBER")
	if bbn != "" {
		return bbn
	}

	return fmt.Sprintf("%d", time.Now().Unix())
}

// LoadEnvsVars construct the vars passed to the step
func LoadEnvsVars(w config.WorkDir) bitbucketVars {
	return bitbucketVars{
		BitbucketCloneDir:          "/opt/atlassian/pipelines/agent/build",
		"BITBUCKET_COMMIT":         getBitbucketCommit(w),
		"BITBUCKET_BUILD_NUMBER":   getBitbucketBuildNumber(),
		"BITBUCKET_REPO_FULL_NAME": "repo-name",
		"BITBUCKET_BRANCH":         "branch-name",
	}
}
