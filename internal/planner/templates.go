package planner

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"text/template"
)

const entrypoint = `#!/bin/bash
#set -e

sleep 1

__LP_COLOR_NC='\033[0m'
__LP_COLOR_PURPLE='\033[0;95m'
__LP_COLOR_YELLOW='\033[0;93m'
__LP_COLOR_RED='\033[0;91m'
__LP_COLOR_GREEN='\033[0;92m'

echo ""
echo ""

echo -e "${__LP_COLOR_YELLOW}*****************************************************${__LP_COLOR_NC}"
echo -e "${__LP_COLOR_YELLOW}****** Step start: {{.name}} ${__LP_COLOR_NC}"
echo -e "${__LP_COLOR_YELLOW}*****************************************************${__LP_COLOR_NC}"

echo ""
echo ""

/localpipeline/entrypoint/script.sh

export BITBUCKET_EXIT_CODE=$?

{{if .after}}
/localpipeline/entrypoint/after-script.sh
{{else}}
true
{{end}}

if [[ "$BITBUCKET_EXIT_CODE" -eq "0" && "$?" -eq "0" ]]; then
	echo -e "${__LP_COLOR_GREEN}*****************************************************${__LP_COLOR_NC}"
	echo -e "${__LP_COLOR_GREEN}****** Step result: Ok ${__LP_COLOR_NC}"
	echo -e "${__LP_COLOR_GREEN}*****************************************************${__LP_COLOR_NC}"
else
	echo -e "${__LP_COLOR_GREEN}*****************************************************${__LP_COLOR_NC}"
	echo -e "${__LP_COLOR_RED}****** Step result: Err ${__LP_COLOR_NC}"
	echo -e "${__LP_COLOR_GREEN}*****************************************************${__LP_COLOR_NC}"
fi
`

const scriptSh = `#!/bin/bash

__LP_COLOR_NC='\033[0m'
__LP_COLOR_PURPLE='\033[0;95m'
__LP_COLOR_YELLOW='\033[0;93m'
__LP_COLOR_RED='\033[0;91m'
__LP_COLOR_GREEN='\033[0;92m'

{{ range .cmds }}
echo -e "${__LP_COLOR_YELLOW}SCRIPT=>${__LP_COLOR_PURPLE}{{.Label}}${__LP_COLOR_NC}"
{{ if .HasLabel2 }}
echo -e "{{.Label2}}"
{{end}}
{{.Script}}
if [[ $? -ne 0 ]]; then
	exit $?
fi
echo ""
echo ""
{{ end }}

`

const afterScriptSh = `#!/bin/bash

__LP_COLOR_NC='\033[0m'
__LP_COLOR_PURPLE='\033[0;95m'
__LP_COLOR_YELLOW='\033[0;93m'
__LP_COLOR_RED='\033[0;91m'
__LP_COLOR_GREEN='\033[0;92m'

{{ range .cmds }}
echo -e "${__LP_COLOR_YELLOW}AFTER-SCRIPT=>${__LP_COLOR_PURPLE}{{.Label}}${__LP_COLOR_NC}"
{{ if .HasLabel2 }}
echo -e "{{.Label2}}"
{{end}}
{{.Script}}
if [[ $? -ne 0 ]]; then
	exit $?
fi
echo ""
echo ""
{{ end }}

`

type Templater struct{}

func (x Templater) templateApply(tmpl string, dest io.Writer, data map[string]interface{}) error {
	t, err := template.New("").Parse(tmpl)
	if err != nil {
		return fmt.Errorf("fail load template: %w", err)
	}
	err = t.Execute(dest, data)
	if err != nil {
		return fmt.Errorf("fail template: %w", err)
	}
	return nil
}

func (x Templater) templateEntrypoint(dest io.WriteCloser, data map[string]interface{}) error {
	return x.templateApply(entrypoint, dest, data)
}

func (x Templater) templateScript(dest io.WriteCloser, data map[string]interface{}) error {
	return x.templateApply(scriptSh, dest, data)
}

func (x Templater) templateAfterscript(dest io.WriteCloser, data map[string]interface{}) error {
	return x.templateApply(afterScriptSh, dest, data)
}

type Filer struct{}

func (x Filer) createFile(name string) io.WriteCloser {
	f, err := os.Create(name)
	if err != nil {
		panic(err) //return fmt.Errorf("fail create: %w", err)
	}
	err = f.Chmod(0755)
	if err != nil {
		panic(err) //return fmt.Errorf("fail chmod: %w", err)
	}
	return f
}

func (x Filer) MkdirAll(name string, perm fs.FileMode) error {
	return os.MkdirAll(name, perm)
}
