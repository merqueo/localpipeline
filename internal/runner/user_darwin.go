//go:build darwin

package runner

import (
	"fmt"
	"os/user"

	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"github.com/ory/dockertest/v3"
)

func getUser(u *user.User) string {
	return ""
}

// copy from
// https://mingheng.medium.com/solving-permission-denied-while-trying-to-connect-to-docker-daemon-socket-from-container-in-mac-os-600c457f1276
func dockerInDocker(s *planner.Step, d *dockertest.Pool, n *dockertest.Network) (*dockertest.Resource, error) {

	s.Env["DOCKER_HOST"] = fmt.Sprintf("tcp://%s-proxy:2375", s.Prefix)

	return d.RunWithOptions(&dockertest.RunOptions{
		Name:       fmt.Sprintf("%s-proxy", s.Prefix),
		Repository: "bpack/socat",
		Tag:        "latest",
		Networks:   []*dockertest.Network{n},
		Cmd: []string{
			"TCP4-LISTEN:2375,fork,reuseaddr", "UNIX-CONNECT:/var/run/docker.sock",
		},
		ExposedPorts: []string{
			"2375",
		},
		Mounts: []string{
			"/var/run/docker.sock:/var/run/docker.sock",
		},
	})
}
