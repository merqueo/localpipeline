package runner

import (
	"fmt"
	"os"
	"os/user"
	"strings"
	"time"

	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/sirupsen/logrus"
)

type Docker struct {
	user       *user.User
	pool       *dockertest.Pool
	logger     *logrus.Logger
	downloaded map[string]bool
}

func (d *Docker) FixMounts(m []string, n string, t string) ([]string, error) {
	image := fmt.Sprintf("%s:%s", n, t)
	if _, ok := d.downloaded[image]; !ok {
		// update image
		err := d.pool.Client.PullImage(docker.PullImageOptions{
			Repository:        n,
			Tag:               t,
			OutputStream:      os.Stderr,
			InactivityTimeout: time.Second * 60,
		}, docker.AuthConfiguration{})
		if err != nil {
			return nil, err
		}
		d.downloaded[image] = true
	}
	// get image info
	ii, err := d.pool.Client.InspectImage(image)
	if err != nil {
		return nil, err
	}

	// fix mount points
	home := ":/root/"
	if ii.ContainerConfig.User != "" && ii.ContainerConfig.User != "root" {
		home = fmt.Sprintf(":/home/%s/", ii.ContainerConfig.User)
	} else if ii.Config.User != "" && ii.Config.User != "root" {
		home = fmt.Sprintf(":/home/%s/", ii.Config.User)
	}

	mm := make([]string, len(m))
	for i, j := range m {
		j = strings.Replace(j, ":$HOME/", home, 1)
		mm[i] = strings.Replace(j, ":~/", home, 1)
	}
	return mm, nil
}

func (d *Docker) Pipeline(s *planner.Step, network *dockertest.Network) error {

	defer network.Close()

	err := s.KnownHosts()
	if err != nil {
		return err
	}

	resourceProxy, err := dockerInDocker(s, d.pool, network)
	if err != nil {
		return err
	}
	if resourceProxy != nil {
		defer resourceProxy.Close()
	}

	mounts, err := d.FixMounts(s.MountsCommon, s.ImageRepository, s.ImageTag)
	if err != nil {
		return err
	}

	mountsMain, err := d.FixMounts(s.MountsMain, s.ImageRepository, s.ImageTag)
	if err != nil {
		return err
	}
	mounts = append(mounts, mountsMain...)

	for _, m := range mounts {
		fmt.Println("mount:", m)
	}

	err = s.Entrypoint(d)
	if err != nil {
		return err
	}

	resource, err := d.pool.RunWithOptions(&dockertest.RunOptions{
		Name:       s.ContainerName,
		Repository: s.ImageRepository,
		Tag:        s.ImageTag,
		Env:        s.Env.ToArray(),
		Cmd:        []string{"/localpipeline/entrypoint/entrypoint.sh"},
		Mounts:     mounts,
		WorkingDir: s.Env[planner.BitbucketCloneDir],
		// User:       getUser(d.user), // se deshabilita para que el contenedor corra como root
		Networks:   []*dockertest.Network{network},
		Tty:        true,
		Entrypoint: []string{"/bin/bash"},
	}, func(hostConfig *docker.HostConfig) {
		hostConfig.Memory = s.RAM
		hostConfig.MemorySwap = s.RAM
		hostConfig.MemorySwappiness = 0
		hostConfig.AutoRemove = true
		hostConfig.PublishAllPorts = false
	})
	if err != nil {
		return err
	}
	defer resource.Close()

	for _, sss := range s.Services {
		resourcex, err := d.pool.RunWithOptions(&dockertest.RunOptions{
			Name:       sss.ContainerName,
			Repository: sss.ImageRepository,
			Tag:        sss.ImageTag,
			Env:        sss.Env,
		}, func(hostConfig *docker.HostConfig) {
			hostConfig.Memory = sss.RAM
			hostConfig.MemorySwap = sss.RAM
			hostConfig.MemorySwappiness = 0
			hostConfig.AutoRemove = true
			hostConfig.PublishAllPorts = false
			hostConfig.NetworkMode = fmt.Sprintf("container:%s", resource.Container.ID)
		})
		if err != nil {
			return fmt.Errorf("container %s fail: %w", sss.ContainerName, err)
		}

		defer resourcex.Close()
	}

	err = d.pool.Client.AttachToContainer(docker.AttachToContainerOptions{
		Container:   resource.Container.ID,
		Stream:      true,
		RawTerminal: true,

		Stdout:       true,
		OutputStream: os.Stdout,

		Stderr:      true,
		ErrorStream: os.Stderr,

		// Stdin:       true,
		// InputStream: os.Stdin,
	})
	if err != nil {
		return err
	}
	return nil
}

func (d *Docker) OnStart(prefix string) (*dockertest.Network, error) {
	// clean containers
	cls, err := d.pool.Client.ListContainers(docker.ListContainersOptions{})
	if err != nil {
		return nil, err
	}
	for _, ct := range cls {
		if strings.HasPrefix(ct.Names[0], "/"+prefix) {
			err := d.pool.Client.RemoveContainer(docker.RemoveContainerOptions{
				ID:    ct.ID,
				Force: true,
			})
			if err != nil {
				return nil, err
			}
		}
	}

	// clean networks
	nets, err := d.pool.Client.ListNetworks()
	if err != nil {
		return nil, err
	}
	for _, n := range nets {
		if strings.HasPrefix(n.Name, prefix) {
			d.pool.Client.RemoveNetwork(n.ID)
		}
	}

	//create new network
	network, err := d.pool.CreateNetwork(fmt.Sprintf("%s-net", prefix))
	if err != nil {
		return nil, fmt.Errorf("network creation failed: %w", err)
	}

	return network, nil
}

func NewDocker(u *user.User, logger *logrus.Logger) (*Docker, error) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, fmt.Errorf("pool creation failed: %w", err)
	}

	return &Docker{
		user:       u,
		pool:       pool,
		logger:     logger,
		downloaded: make(map[string]bool),
	}, nil
}
