//go:build linux

package runner

import (
	"fmt"
	"os"
	"os/user"
	"syscall"

	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"github.com/ory/dockertest/v3"
)

func getUser(u *user.User) string {
	var gid string
	dsinfo, err := os.Stat("/var/run/docker.sock")
	if err == nil {
		if stat, ok := dsinfo.Sys().(*syscall.Stat_t); ok {
			gid = fmt.Sprintf("%d", stat.Gid)
		}
	} else {
		fmt.Println(err)
		gid = u.Gid
	}
	return fmt.Sprintf("%s:%s", u.Uid, gid)
}

func dockerInDocker(s *planner.Step, _ *dockertest.Pool, _ *dockertest.Network) (*dockertest.Resource, error) {
	s.MountsCommon = append(s.MountsCommon, "/var/run/docker.sock:/var/run/docker.sock")
	return nil, nil
}
