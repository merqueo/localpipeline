package executor

import (
	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"github.com/ory/dockertest/v3"
)

type Docker interface {
	OnStart(string) (*dockertest.Network, error)
	Pipeline(*planner.Step, *dockertest.Network) error
}

type Executor struct {
	docker Docker
	steps  []*planner.Step
}

func NewExecutor(docker Docker, steps []*planner.Step) *Executor {
	return &Executor{
		docker: docker,
		steps:  steps,
	}
}

func (e *Executor) Execute() error {
	for _, s := range e.steps {
		n, err := e.docker.OnStart(s.Prefix)
		if err != nil {
			return err
		}
		err = e.docker.Pipeline(s, n)
		if err != nil {
			return err
		}
	}
	return nil
}
