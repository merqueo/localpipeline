package executor

import (
	"errors"
	"testing"

	"bitbucket.org/merqueo/localpipeline/internal/planner"
	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/mock"
)

type DockerMock struct {
	mock.Mock
}

func (m *DockerMock) OnStart(prefix string) (*dockertest.Network, error) {
	args := m.Called(prefix)
	return args.Get(0).(*dockertest.Network), args.Error(1)
}

func (m *DockerMock) Pipeline(s *planner.Step, n *dockertest.Network) error {
	args := m.Called(s, n)
	return args.Error(0)
}

func TestExecutor_Execute(t *testing.T) {
	type fields struct {
		docker *DockerMock
		steps  []*planner.Step
	}
	tests := []struct {
		name    string
		fields  fields
		mocker  func(fields)
		wantErr bool
	}{
		{
			name: "ok",
			fields: fields{
				docker: &DockerMock{},
				steps: []*planner.Step{
					{
						Prefix: "xxxx",
					},
				},
			},
			mocker: func(f fields) {
				n := &dockertest.Network{}
				f.docker.On("OnStart", "xxxx").Return(n, nil).Once()
				f.docker.On("Pipeline", f.steps[0], n).Return(nil).Once()
			},
			wantErr: false,
		},
		{
			name: "fail OnStart",
			fields: fields{
				docker: &DockerMock{},
				steps: []*planner.Step{
					{
						Prefix: "xxxx",
					},
				},
			},
			mocker: func(f fields) {
				f.docker.On("OnStart", "xxxx").Return(new(dockertest.Network), errors.New("ups")).Once()
			},
			wantErr: true,
		},
		{
			name: "fail pipeline",
			fields: fields{
				docker: &DockerMock{},
				steps: []*planner.Step{
					{
						Prefix: "xxxx",
					},
				},
			},
			mocker: func(f fields) {
				n := &dockertest.Network{}
				f.docker.On("OnStart", "xxxx").Return(n, nil).Once()
				f.docker.On("Pipeline", f.steps[0], n).Return(errors.New("ups")).Once()
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mocker(tt.fields)
			e := NewExecutor(tt.fields.docker, tt.fields.steps)
			if err := e.Execute(); (err != nil) != tt.wantErr {
				t.Errorf("Executor.Execute() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.fields.docker.AssertExpectations(t)
		})
	}
}
