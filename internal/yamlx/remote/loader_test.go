package remote

import (
	"testing"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
	"github.com/d4l3k/messagediff"
)

func TestLoad(t *testing.T) {
	type args struct {
		w config.WorkDir
	}
	tests := []struct {
		name    string
		args    args
		want    Config
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				w: "testdata-1",
			},
			want: Config{
				Image: "golang:1.18-bullseye",
				Definitions: Definitions{
					Caches: map[string]string{"go-custom": "/go"},
					Services: Services{
						"mysql": Service{
							Image:  "mysql:5.7",
							Memory: 1024,
							Envs: load.EnvMap{
								"MYSQL_DATABASE":      "test",
								"MYSQL_ROOT_PASSWORD": "toor",
							},
						},
					},
				},
				Pipelines: Pipelines{
					Default: Branch{
						BranchStep{
							Step: &Step{
								Name: "testing",
								Caches: []string{
									"go-custom",
								},
								Scripts: ScriptList{
									`echo "hola"`,
								},
								Services: []string{
									"mysql",
								},
								Size: "2x",
							},
						},
					},
					Branches: map[string]Branch{
						"master": {
							BranchStep{
								Step: &Step{
									Name: "testing",
									Caches: []string{
										"go-custom",
									},
									Scripts: ScriptList{
										`echo "hola"`,
									},
									Services: []string{
										"mysql",
									},
									Size: "2x",
								},
							},
							BranchStep{
								Parallel: &StepList{
									&StepInStep{
										Step: &Step{
											Image: "xxxx1",
											Name:  "deploy 1",
											// 		Caches:     []string{},
											// 		Size:       "",
											// 		Services:   []string{},
											Deployment: "production-1",
											Scripts: ScriptList{
												"echo 1",
											},
											AfterScripts: ScriptList{
												"echo 1a",
											},
										},
									},
									&StepInStep{
										Step: &Step{
											Image: "xxxx2",
											Name:  "deploy 2",
											// 		Caches:     []string{},
											// 		Size:       "",
											// 		Services:   []string{},
											Deployment: "production-2",
											Scripts: ScriptList{
												"echo 2",
											},
											AfterScripts: ScriptList{
												"echo 2a",
											},
										},
									},
								},
							},
						},
					},
					Tags: map[string]Branch{
						"v*": {
							BranchStep{
								Step: &Step{
									Name: "testing",
									Caches: []string{
										"go-custom",
									},
									Scripts: ScriptList{
										`echo "hola"`,
									},
									Services: []string{
										"mysql",
									},
									Size: "2x",
								},
							},
							BranchStep{
								Parallel: &StepList{
									&StepInStep{
										Step: &Step{
											Image: "xxxx3",
											Name:  "deploy 3",
											// 		Caches:     []string{},
											// 		Size:       "",
											// 		Services:   []string{},
											Deployment: "production-3",
											Scripts: ScriptList{
												"echo 3",
											},
											AfterScripts: ScriptList{
												"echo 3a",
											},
										},
									},
									&StepInStep{
										Step: &Step{
											Image: "xxxx4",
											Name:  "deploy 4",
											// 		Caches:     []string{},
											// 		Size:       "",
											// 		Services:   []string{},
											Deployment: "production-4",
											Scripts: ScriptList{
												"echo 4",
											},
											AfterScripts: ScriptList{
												"echo 4a",
											},
										},
									},
								},
							},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "fail not found",
			args: args{
				w: "testdata-0",
			},
			wantErr: true,
		},
		{
			name: "fail bad yaml",
			args: args{
				w: "testdata-2",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Load(tt.args.w)
			if (err != nil) != tt.wantErr {
				t.Errorf("Load() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff, equal := messagediff.PrettyDiff(tt.want, got); !equal {
				t.Errorf("Something() = %#v\n%s", got, diff)
			}

			if err == nil {
				if got.Image.Name() != "golang" {
					t.Errorf("imagen name invalid: %s expected %s", got.Image.Name(), "golang")
				}
				if got.Image.Tag() != "1.18-bullseye" {
					t.Errorf("imagen name invalid: %s expected %s", got.Image.Tag(), "1.18-bullseye")
				}

				ls := got.Pipelines.Branches["master"].List()
				for _, s := range ls {
					if !s.Is("deploy 1") {
						continue
					}
					if ls[1].Image.Name() != "xxxx1" {
						t.Errorf("imagen name invalid: %s expected %s", got.Image.Name(), "xxxx1")
					}
					if ls[1].Image.Tag() != "latest" {
						t.Errorf("imagen name invalid: %s expected %s", ls[1].Image.Tag(), "latest")
					}
				}
			}

		})
	}
}
