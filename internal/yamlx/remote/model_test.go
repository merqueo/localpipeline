package remote

import (
	"reflect"
	"testing"
)

func TestPipelines_Steps(t *testing.T) {
	type fields struct {
		Branches map[string]Branch
		Tags     map[string]Branch
	}
	type args struct {
		branch string
		tag    string
		filter []string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []*Step
	}{
		{
			name: "by branch",
			fields: fields{
				Branches: map[string]Branch{
					"master": {
						BranchStep{
							Step: &Step{
								Name: "step-1",
							},
						},
						BranchStep{
							Step: &Step{
								Name: "step-2",
							},
						},
						BranchStep{
							Parallel: &StepList{
								&StepInStep{
									Step: &Step{
										Name: "step-3",
									},
								},
								&StepInStep{
									Step: &Step{
										Name: "step-4",
									},
								},
							},
						},
					},
				},
			},
			args: args{
				branch: "master",
				filter: []string{"step-1", "step-3"},
			},
			want: []*Step{
				{
					Name: "step-1",
				},
				{
					Name: "step-3",
				},
			},
		},
		{
			name: "by tag",
			fields: fields{
				Tags: map[string]Branch{
					"v*": {
						BranchStep{
							Step: &Step{
								Name: "step-4",
							},
						},
						BranchStep{
							Step: &Step{
								Name: "step-3",
							},
						},
						BranchStep{
							Parallel: &StepList{
								&StepInStep{
									Step: &Step{
										Name: "step-2",
									},
								},
								&StepInStep{
									Step: &Step{
										Name: "step-1",
									},
								},
							},
						},
					},
				},
			},
			args: args{
				tag:    "v*",
				filter: []string{"step-2", "step-4"},
			},
			want: []*Step{
				{
					Name: "step-2",
				},
				{
					Name: "step-4",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := Pipelines{
				Branches: tt.fields.Branches,
				Tags:     tt.fields.Tags,
			}
			if got := p.Steps(tt.args.branch, tt.args.tag, tt.args.filter); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pipelines.Steps() = %v, want %v", got, tt.want)
			}
		})
	}
}
