package remote

import (
	"strings"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/load"
)

type ScriptList []interface{}

type Image string

func (i Image) Name() string {
	return strings.Split(string(i), ":")[0]
}

func (i Image) Tag() string {
	p := strings.Split(string(i), ":")
	if len(p) == 1 {
		return "latest"
	}
	return p[1]
}

type Service struct {
	Image  Image       `yaml:"image"`
	Memory int64       `yaml:"memory"`
	Envs   load.EnvMap `yaml:"variables"`
}

type Services map[string]Service

type Definitions struct {
	Caches   map[string]string `yaml:"caches"`
	Services Services          `yaml:"services"`
}

type Step struct {
	Image        Image      `yaml:"image"`
	Name         string     `yaml:"name"`
	Caches       []string   `yaml:"caches"`
	Size         string     `yaml:"size"`
	Services     []string   `yaml:"services"`
	Deployment   string     `yaml:"deployment"`
	Scripts      ScriptList `yaml:"script"`
	AfterScripts ScriptList `yaml:"after-script"`
}

func (s Step) Is(name string) bool {
	return s.Name == name
}

type StepInStep struct {
	Step *Step `yaml:"step"`
}

type StepList []*StepInStep

func (s StepList) List() []*Step {
	l := make([]*Step, 0)
	for _, ss := range s {
		l = append(l, ss.Step)
	}
	return l
}

type BranchStep struct {
	Step     *Step     `yaml:"step"`
	Parallel *StepList `yaml:"parallel,flow"`
}

type Branch []BranchStep

func (b Branch) List() []*Step {
	l := make([]*Step, 0)
	for _, bs := range b {
		if bs.Step != nil {
			l = append(l, bs.Step)
		}
		if bs.Parallel != nil {
			l = append(l, bs.Parallel.List()...)
		}
	}
	return l
}

type Pipelines struct {
	Default  Branch            `yaml:"default,flow"`
	Branches map[string]Branch `yaml:"branches,flow"`
	Tags     map[string]Branch `yaml:"tags,flow"`
}

func (p Pipelines) Steps(branch, tag string, filter []string) []*Step {
	var s []*Step
	if br, ok := p.Branches[branch]; ok {
		s = br.List()
	} else if br, ok := p.Tags[tag]; ok {
		s = br.List()
	}
	ss := make([]*Step, 0)
	for _, f1 := range filter {
		for _, f2 := range s {
			if f1 == f2.Name {
				ss = append(ss, f2)
				break
			}
		}
	}

	return ss
}

type Config struct {
	Image       Image       `yaml:"image"`
	Definitions Definitions `yaml:"definitions"`
	Pipelines   Pipelines   `yaml:"pipelines,flow"`
}
