package remote

import (
	"fmt"
	"os"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"gopkg.in/yaml.v2"
)

func Load(w config.WorkDir) (Config, error) {

	var c Config

	f, err := os.Open(fmt.Sprintf("%s/bitbucket-pipelines.yml", w))
	if err != nil {
		return Config{}, err
	}

	err = yaml.NewDecoder(f).Decode(&c)
	if err != nil {
		return Config{}, err
	}

	return c, nil
}
