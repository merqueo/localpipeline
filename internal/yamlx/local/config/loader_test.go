// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package config

import (
	"os/user"
	"reflect"
	"testing"

	"github.com/d4l3k/messagediff"
)

func TestLoad(t *testing.T) {
	type args struct {
		runner  string
		workdir WorkDir
		user    *user.User
	}
	tests := []struct {
		name     string
		args     args
		envs     map[string]string
		want     *Config
		wantFail bool
		wantErr  error
	}{
		{
			name: "test ok branch",
			args: args{
				runner:  "runner-2",
				workdir: "./testdata-1",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs: map[string]string{
				"MY_ENVVAR": "staging-1",
			},
			want: &Config{
				Name:         "awesome-project",
				WorkDir:      "./testdata-1",
				StoreDir:     "/home/user/.cache/localpipeline/awesome-project",
				LocalYaml:    "./testdata-1/bitbucket-local.yml",
				RunnerName:   "runner-2",
				RunnerBranch: "develop",
				RunnerSteps: []string{
					"step-c",
					"step-d",
				},
				Vars: map[string]interface{}{
					"ENV": map[string]interface{}{
						"MY_ENVVAR": "staging-1",
					},
					"PROJECT_NAME": "awesome-project",
					"HOME":         "/home/user",
				},
			},
			wantFail: false,
			wantErr:  nil,
		},
		{
			name: "test ok tag",
			args: args{
				runner:  "runner-4",
				workdir: "./testdata-1",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs: map[string]string{
				"MY_ENVVAR": "staging-1",
			},
			want: &Config{
				Name:       "awesome-project",
				WorkDir:    "./testdata-1",
				StoreDir:   "/home/user/.cache/localpipeline/awesome-project",
				LocalYaml:  "./testdata-1/bitbucket-local.yml",
				RunnerName: "runner-4",
				RunnerTag:  "v*",
				RunnerSteps: []string{
					"step-g",
				},
				Vars: map[string]interface{}{
					"ENV": map[string]interface{}{
						"MY_ENVVAR": "staging-1",
					},
					"PROJECT_NAME": "awesome-project",
					"HOME":         "/home/user",
				},
			},
			wantFail: false,
			wantErr:  nil,
		},
		{
			name: "test fail bitbucket-local.yml not found",
			args: args{
				runner:  "runner-xxxxxxx",
				workdir: "./testdata-0",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs: map[string]string{
				"MY_ENVVAR": "staging-1",
			},
			wantFail: true,
		},
		{
			name: "test fail bitbucket-local.yml invalid",
			args: args{
				runner:  "runner-xxxxxxx",
				workdir: "./testdata-2",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs: map[string]string{
				"MY_ENVVAR": "staging-1",
			},
			wantFail: true,
		},
		{
			name: "test fail bitbucket-local.yml envvar not defined",
			args: args{
				runner:  "runner-xxxxxxx",
				workdir: "./testdata-1",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs:     map[string]string{},
			wantFail: true,
			wantErr:  &EnvvarNotFoundError{"MY_ENVVAR"},
		},
		{
			name: "test fail runner not found",
			args: args{
				runner:  "runner-xxxxxxx",
				workdir: "./testdata-1",
				user: &user.User{
					HomeDir: "/home/user",
				},
			},
			envs: map[string]string{
				"MY_ENVVAR": "staging-1",
			},
			wantFail: true,
			wantErr: &RunnerNotFoundError{
				"runner-xxxxxxx",
				[]string{"runner-1", "runner-2", "runner-3", "runner-4"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for k, v := range tt.envs {
				t.Setenv(k, v)
			}
			got, err := Load(tt.args.runner, tt.args.workdir, tt.args.user)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Preload() = %v, want %v", got, tt.want)
			}

			if (err != nil) != tt.wantFail {
				t.Errorf("Preload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr != nil {
				if diff, equal := messagediff.PrettyDiff(tt.wantErr, err); !equal {
					t.Errorf("err = %#v\n%s", got, diff)
				}
			}
		})
	}
}
