// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package config preload the initial config from bitbucket-local.yml
package config

import (
	"fmt"
	"strings"
)

// WorkDir directory with data
type WorkDir string

// Config stores the information load from host and the bitbucket-local.yml
type Config struct {
	Name         string                 // the project name
	WorkDir      WorkDir                // current directory
	StoreDir     string                 // cache directory
	LocalYaml    string                 // path to bitbucket-local.yml
	RunnerName   string                 // name of runner to execute
	RunnerBranch string                 // name of the branch in bitbucket-pipelines.yml
	RunnerTag    string                 // name of the tag in bitbucket-pipelines.yml
	RunnerSteps  []string               // steps in the branch to run
	Vars         map[string]interface{} // Vars loaded from env and from bitbucket-local.yml
}

type RunnerNotFoundError struct {
	runner    string
	available []string
}

func (e *RunnerNotFoundError) Error() string {
	return fmt.Sprintf(`runner "%s" not found, runners available: %s`, e.runner, strings.Join(e.available, ", "))
}

type EnvvarNotFoundError struct {
	envvar string
}

func (e *EnvvarNotFoundError) Error() string {
	return fmt.Sprintf(`envvar "%s" not found`, e.envvar)
}

// tmpmodel struct to load main data from bitbucket-local.yml file
type tmpmodel struct {
	Project struct {
		Name string   `yaml:"name"`
		Env  []string `yaml:"env"`
	} `yaml:"project"`
	Runners []struct {
		Name   string `yaml:"name"`
		Branch string `yaml:"branch"`
		Tag    string `yaml:"tag"`
		Steps  []struct {
			Name string `yaml:"name"`
		}
	} `yaml:"runners"`
}
