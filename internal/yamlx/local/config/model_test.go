// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package config preload the initial config from bitbucket-local.yml
package config

import (
	"testing"
)

func TestRunnerNotFoundError_Error(t *testing.T) {
	type fields struct {
		runner    string
		available []string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "ok",
			fields: fields{
				runner:    "r1",
				available: []string{"r2", "r3"},
			},
			want: `runner "r1" not found, runners available: r2, r3`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &RunnerNotFoundError{
				runner:    tt.fields.runner,
				available: tt.fields.available,
			}
			if got := e.Error(); got != tt.want {
				t.Errorf("RunnerNotFoundError.Error() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEnvvarNotFoundError_Error(t *testing.T) {
	type fields struct {
		envvar string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "ok",
			fields: fields{
				envvar: "XX",
			},
			want: `envvar "XX" not found`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &EnvvarNotFoundError{
				envvar: tt.fields.envvar,
			}
			if got := e.Error(); got != tt.want {
				t.Errorf("EnvvarNotFoundError.Error() = %v, want %v", got, tt.want)
			}
		})
	}
}
