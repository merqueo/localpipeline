// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package config preload the initial config from bitbucket-local.yml
package config

import (
	"fmt"
	"os"
	"os/user"

	"gopkg.in/yaml.v2"
)

// Load
func Load(runner string, workDir WorkDir, user *user.User) (*Config, error) {
	localYaml := fmt.Sprintf("%s/bitbucket-local.yml", workDir)
	file, err := os.Open(localYaml)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var m tmpmodel
	err = yaml.NewDecoder(file).Decode(&m)
	if err != nil {
		return nil, err
	}

	// find in the os the env vars defined in the yaml file
	envs := make(map[string]interface{})
	for _, e := range m.Project.Env {
		v := os.Getenv(e)
		if v == "" {
			return nil, &EnvvarNotFoundError{e}
		}
		envs[e] = v
	}
	vars := make(map[string]interface{})
	vars["ENV"] = envs
	vars["PROJECT_NAME"] = m.Project.Name
	vars["HOME"] = user.HomeDir

	var branch string
	var tag string
	var steps []string
	for _, r := range m.Runners {
		if r.Name == runner && r.Branch != "" {
			branch = r.Branch
			steps = make([]string, len(r.Steps))
			for i, s := range r.Steps {
				steps[i] = s.Name
			}
			break
		}
	}
	for _, r := range m.Runners {
		if r.Name == runner && r.Tag != "" {
			tag = r.Tag
			steps = make([]string, len(r.Steps))
			for i, s := range r.Steps {
				steps[i] = s.Name
			}
			break
		}
	}
	if branch == "" && tag == "" {
		available := make([]string, len(m.Runners))
		for i, runner := range m.Runners {
			available[i] = runner.Name
		}
		return nil, &RunnerNotFoundError{
			runner,
			available,
		}
	}

	return &Config{
		Name:         m.Project.Name,
		WorkDir:      workDir,
		StoreDir:     fmt.Sprintf("%s/.cache/localpipeline/%s", user.HomeDir, m.Project.Name),
		LocalYaml:    localYaml,
		RunnerName:   runner,
		RunnerBranch: branch,
		RunnerTag:    tag,
		RunnerSteps:  steps,
		Vars:         vars,
	}, nil
}
