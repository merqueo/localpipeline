// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package load read and unpack config from bitbucket-local.yml
package load

import (
	"reflect"
	"sort"
	"testing"
)

func TestEnvs_Process(t *testing.T) {
	type fields struct {
		Vars  EnvMap
		Files []string
	}
	tests := []struct {
		name    string
		fields  fields
		want    EnvMap
		wantErr bool
	}{
		{
			name: "ok",
			fields: fields{
				Vars: EnvMap{
					"XX": "YY",
				},
				Files: []string{
					"testdata-1/env1.txt",
					"testdata-1/env2.txt",
				},
			},
			want: EnvMap{
				"XX":  "YY",
				"AAA": "BBB",
				"CCC": "DDD",
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				Vars: EnvMap{
					"XX": "YY",
				},
				Files: []string{
					"testdata-1/env1.txtxxxxxxxx",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Envs{
				Vars:  tt.fields.Vars,
				Files: tt.fields.Files,
			}
			got, err := l.Process()
			if (err != nil) != tt.wantErr {
				t.Errorf("Envs.Process()  error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Envs.Process() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEnvMap_ToArray(t *testing.T) {
	tests := []struct {
		name string
		e    EnvMap
		want []string
	}{
		{
			name: "ok",
			e: EnvMap{
				"A": "a",
				"B": "b",
			},
			want: []string{
				"A=a",
				"B=b",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.e.ToArray()
			sort.Slice(got, func(i, j int) bool {
				return got[j] > got[i]
			})
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EnvMap.ToArray() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEnvMap_Add(t *testing.T) {
	type args struct {
		m map[string]string
	}
	tests := []struct {
		name string
		e    EnvMap
		args args
		want EnvMap
	}{
		{
			name: "ok",
			e: EnvMap{
				"A": "a",
			},
			args: args{
				m: map[string]string{
					"B": "b",
				},
			},
			want: EnvMap{
				"A": "a",
				"B": "b",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.e.Add(tt.args.m)
			if !reflect.DeepEqual(tt.e, tt.want) {
				t.Errorf("EnvMap.ToArray() = %v, want %v", tt.e, tt.want)
			}
		})
	}
}

func TestRunners_Find(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		l    Runners
		args args
		want *Runner
	}{
		{
			name: "ok",
			l: Runners{
				Runner{
					Name: "a",
				},
				Runner{
					Name: "b",
				},
			},
			args: args{
				name: "b",
			},
			want: &Runner{
				Name: "b",
			},
		},
		{
			name: "not found",
			l: Runners{
				Runner{
					Name: "a",
				},
				Runner{
					Name: "b",
				},
			},
			args: args{
				name: "c",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.Find(tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Runners.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStepList_Filter(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		s    StepList
		args args
		want Step
	}{
		{
			name: "empty",
			s:    StepList{},
			args: args{
				name: "step-1",
			},
			want: Step{},
		},
		{
			name: "find 1",
			s: StepList{
				{
					Name: "step-1",
				},
				{
					Name: "step-2",
				},
				{
					Name: "step-3",
				},
			},
			args: args{
				name: "step-2",
			},
			want: Step{
				Name: "step-2",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Filter(tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StepList.Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}
