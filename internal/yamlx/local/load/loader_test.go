package load

import (
	"os/user"
	"testing"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"github.com/d4l3k/messagediff"
)

func configure(t *testing.T, r string, w config.WorkDir) *config.Config {
	t.Setenv("XXX_YYY", "abcd")
	c, err := config.Load(r, w, &user.User{HomeDir: "/home/user"})
	if err != nil {
		t.Error(err)
	}
	c.Vars["TESTING"] = EnvMap{
		"LOCAL_CI_SERVICE_MYSQL": "127.0.0.1",
	}
	return c
}

func TestLoad(t *testing.T) {
	type args struct {
		c *config.Config
	}
	tests := []struct {
		name    string
		args    args
		want    Config
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				c: configure(t, "artifact", "testdata-1"),
			},
			want: Config{
				Deployments: Deployments{
					"production": Envs{
						Vars: EnvMap{
							"PR_APP": "awesome-project",
							"PR_ENV": "abcd",
						},
						Files: []string{
							"/home/user/abcd/aws-envs",
						},
					},
				},
				Repository: Envs{
					Vars: EnvMap{
						"AWS_DEFAULT_REGION": "us-east-1",
						"DEPLOYMENT_CONFIG":  "CodeDeployDefault.OneAtATime",
					},
					Files: []string(nil),
				},
				Runners: Runners{
					Runner{
						Name:   "testing",
						Branch: "master",
						Steps: []Step{
							{
								Name: "testing",
								Mounts: []string{
									"vendor",
								},
							},
						},
					},
					Runner{
						Name: "artifact",
						Tag:  "v*",
						Steps: []Step{
							{
								Name: "build",
								Mounts: []string{
									"vendor",
								},
							},
						},
					},
				},
				SSH: SSH{
					Hosts: []string{
						"github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+" +
							"PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizH" +
							"hbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKS" +
							"CZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJ" +
							"vdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==",
					},
					Key: "/home/user/.ssh/id_rsa",
				},
			},
			wantErr: false,
		},
		{
			name: "fail bad template",
			args: args{
				c: configure(t, "artifact", "testdata-2"),
			},
			wantErr: true,
		},
		{
			name: "fail incomplete data",
			args: args{
				c: configure(t, "artifact", "testdata-3"),
			},
			wantErr: true,
		},
		{
			name: "fail invalid yaml",
			args: args{
				c: configure(t, "artifact", "testdata-4"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got, err := Load(tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("Load() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff, equal := messagediff.PrettyDiff(tt.want, got); !equal {
				t.Errorf("Something() = %#v\n%s", got, diff)
			}
		})
	}
}
