package load

import (
	"bytes"
	"path"
	"text/template"

	"bitbucket.org/merqueo/localpipeline/internal/yamlx/local/config"
	"gopkg.in/yaml.v2"
)

func ta(ts string, vars map[string]interface{}) (*bytes.Buffer, error) {
	buf := bytes.NewBuffer(nil)

	t := template.New(path.Base(ts))
	t, err := t.ParseFiles(ts)
	if err != nil {
		return nil, err
	}

	err = t.Option("missingkey=error").Execute(buf, vars)
	if err != nil {
		return nil, err
	}
	return buf, nil
}

func Load(c *config.Config) (Config, error) {
	var fc Config

	buf, err := ta(c.LocalYaml, c.Vars)
	if err != nil {
		return Config{}, err
	}

	err = yaml.NewDecoder(buf).Decode(&fc)
	if err != nil {
		return Config{}, err
	}
	return fc, nil
}
