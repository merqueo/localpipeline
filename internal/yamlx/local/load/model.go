// Copyright 2022 Juan Enrique Escobar. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package load read and unpack config from bitbucket-local.yml
package load

import (
	"fmt"
	"os"
	"strings"
)

type EnvMap map[string]string

func (e EnvMap) ToArray() []string {
	r := make([]string, len(e))
	i := 0
	for k, v := range e {
		r[i] = fmt.Sprintf("%s=%s", k, v)
		i++
	}
	return r
}

func (e EnvMap) Add(m map[string]string) {
	for k, v := range m {
		e[k] = v
	}
}

type Envs struct {
	Vars  EnvMap   `yaml:"vars"`
	Files []string `yaml:"files"`
}

func (l Envs) Process() (EnvMap, error) {
	e := make(map[string]string)
	for _, f := range l.Files {
		b, err := os.ReadFile(f)
		if err != nil {
			return nil, err
		}
		parts := strings.Split(string(b), "\n")
		for _, p := range parts {
			if p != "" {
				pp := strings.SplitN(p, "=", 2)
				e[pp[0]] = pp[1]
			}
		}
	}
	for k, v := range l.Vars {
		e[k] = v
	}

	return e, nil
}

type SSH struct {
	Hosts []string `yaml:"hosts"`
	Key   string   `yaml:"key"`
}

type Deployments map[string]Envs

type Step struct {
	Name   string   `yaml:"name"`
	Mounts []string `yaml:"mounts"`
}

type StepList []Step

func (s StepList) Filter(name string) Step {
	for _, ss := range s {
		if ss.Name == name {
			return ss
		}
	}
	return Step{}
}

type Runner struct {
	Name   string   `yaml:"name"`
	Branch string   `yaml:"branch"`
	Tag    string   `yaml:"tag"`
	Steps  StepList `yaml:"steps"`
}

type Runners []Runner

func (l Runners) Find(name string) *Runner {
	for _, r := range l {
		if r.Name == name {
			return &r
		}
	}
	return nil
}

type Config struct {
	Deployments Deployments `yaml:"deployments"`
	Repository  Envs        `yaml:"repository"`
	SSH         SSH         `yaml:"ssh"`
	Runners     Runners     `yaml:"runners"`
}
